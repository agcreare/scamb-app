import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import ScrollToTop from './utils/ScrollToTop'

// Views
import Home from './views/Home';
import Product from './views/Product';
import BuyPoints from './views/BuyPoints';
import Buy from './views/Buy';
import Store from './views/Store';
import Help from './views/Help';
import MobileSearch from './views/MobileSearch';
import Results from './views/Results';
import TermsConditions from './views/TermsConditions';
import OfficialStores from './views/OfficialStores';
import HowItWorks from './views/HowItWorks';
import Welcome from './views/auth/Welcome';
import Login from './views/auth/Login';
import CreateAccount from './views/auth/CreateAccount';
import CreateAd from './views/CreateAd';
import MyAds from './views/account/MyAds';
import MyFavorites from './views/account/MyFavorites';
import MyPurchases from './views/account/MyPurchases';
import MySales from './views/account/MySales';
import MyProfile from './views/account/MyProfile';
import Messenger from './views/account/Messenger';
import MyWallet from './views/account/MyWallet';
import ActivityHistoryPage from './views/account/ActivityHistoryPage';

class Routes extends Component {
    
    render () {
        return (

            <ScrollToTop>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/produto" component={Product} />
                <Route exact path="/comprar-pontos" component={BuyPoints} />
                <Route exact path="/comprar" component={Buy} />
                <Route exact path="/minha-loja" component={Store} />
                <Route exact path="/ajuda" component={Help} />
                <Route exact path="/buscar" component={MobileSearch} />
                <Route exact path="/resultados" component={Results} />
                <Route exact path="/lojas-oficiais" component={OfficialStores} />
                <Route exact path="/termos-e-condicoes" component={TermsConditions} />
                <Route exact path="/como-funciona" component={HowItWorks} />
                <Route exact path="/prosseguir" component={Welcome} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/criar-conta" component={CreateAccount} />
                <Route exact path="/criar-novo" component={CreateAd} />
                <Route exact path="/meus-anuncios" component={MyAds} />
                <Route exact path="/minhas-vendas" component={MySales} />
                <Route exact path="/meus-favoritos" component={MyFavorites} />
                <Route exact path="/minhas-compras" component={MyPurchases} />
                <Route exact path="/meu-perfil" component={MyProfile} />
                <Route exact path="/mensagens" component={Messenger} />
                <Route exact path="/minha-carteira" component={MyWallet} />
                <Route exact path="/historico-de-atividades" component={ActivityHistoryPage} />
            </Switch>
            </ScrollToTop>

        )
    
    }
}

export default Routes;