// Jquery
import $ from 'jquery';

$(document).ready(function () { console.log('jquery is on!') });

// ** Initialize scripts here

function runScripts () {
    
    // Loaders..
    owlCarouselSlider();
}

// Function Example..
function owlCarouselSlider () {
    console.log('Owl Carousel is On!');
}

// Run All Scripts..
runScripts();