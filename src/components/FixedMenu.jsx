import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import SideMenu from './SideMenu';
import SideMenuCategories from './SideMenuCategories';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import { faHome, faSearch, faPlusSquare, faCommentAltLines, faInbox } from '@fortawesome/pro-light-svg-icons';

//Imagens
import Avatar from '../assets/images/avatar.png';

class FixedMenu extends Component {

	componentDidMount() {
		let current = null;
		let fixedMenu = $("._ui-fixed-menu-items");

		$(fixedMenu).on('click', '._ui-fixed-menu-item', function () {

			$(this).parents(fixedMenu).find('._active').removeClass('_active').end().end().addClass('_active');

		});

	}

	render() {
		
		return (
			<>
            <SideMenu className="_aside-menu-user _is-hidden"></SideMenu>
			<SideMenuCategories className="_aside-menu-categories _is-hidden"></SideMenuCategories>
            
			<div id="_ui-fixed-menu--wrapper">
				<div className="_ui-fixed-menu-items">
					<div className="_ui-fixed-menu-item _active">
						<Link to="./" className="">
							<FontAwesomeIcon icon={faHome}></FontAwesomeIcon>
						</Link>
					</div>
					<div className="_ui-fixed-menu-item">
						<Link to="/buscar" className="">
							<FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>
						</Link>
					</div>
					<div className="_ui-fixed-menu-item">
						<Link to="/criar-novo" className="">
							<FontAwesomeIcon icon={faPlusSquare}></FontAwesomeIcon>
						</Link>
					</div>
					<div className="_ui-fixed-menu-item">
						<Link to="/mensagens" className="">
							<span className="message-box">
								<FontAwesomeIcon icon={faInbox} />
								<span className="_notification-icon-mark _red"></span>
							</span>
						</Link>
					</div>
					<div className="_ui-fixed-menu-item">
						<button type="button" className="btn btn-transparent _color _black _mobile-nav-user">
							<span className="user-image _rounded-image__with-notification-icon _1x">
								<img src={Avatar} alt="Avatar" />
								<span className="_notification-icon-mark _red"></span>
							</span>
						</button>
					</div>
				</div>
			</div>
			</>
		);
		
	};
	
};

export default FixedMenu;