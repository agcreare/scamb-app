import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import {
        faFacebookF,
        faInstagram,
        faLinkedinIn
       } from '@fortawesome/free-brands-svg-icons';

// Images
import LogoScamb from '../assets/images/logo.png';

class Footer extends Component {
    
    render () {
        
        return (
        
            <div>
                <div className="_wrap-footer">
                
                    <div className="_footer">
                
                        {/* Container */}
                        <div className="_med-container">

                            {/* Cols */}
                            <div className="cols">
                                
                                <div className="_col _col-1">
                                    <div className="inside-col">
                                        <Link to="/" className="logo">
                                            <img src={LogoScamb} alt="" />
                                        </Link>
                                        <p className="_color _black _12px _fw700">
                                            Comprar coisas<br/> pagando com coisas
                                        </p>
                                    </div>
                                </div>
                                
                                <div className="_col _col-2">
                                    <div className="inside-col">
                                        <h1 className="title _color _black _14px _fw700">
                                            Links
                                        </h1>
                                        
                                        <nav className="_list-nav _first-list">
                                        <ul>
                                            <li>
                                                <Link to="/">
                                                    ajuda
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    como vender
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    como comprar
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    marcas
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    termos de uso
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    política de privacidade
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    carreiras
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    promoções
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                    </div>
                                    
                                </div>
                                
                                <div className="_col _col-3">
                                    <div className="inside-col">
                                        <h1 className="title _color _black _14px _fw700">
                                            Meu perfil
                                        </h1>
                                        
                                        <nav className="_list-nav">
                                        <ul>
                                            <li>
                                                <Link to="/">
                                                    perfil
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    meu saldo
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    espalhe o bem
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    meus produtos
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    meus pedidos
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    promoções
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    emails
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                    </div>
                                    
                                </div>
                                
                                <div className="_col _col-4">
                                    <div className="inside-col">
                                        <h1 className="title _color _black _14px _fw700">
                                            Siga o Scamb
                                        </h1>
                                        
                                        <nav className="_list-nav socials">
                                        <ul>
                                            <li>
                                                <Link to="/">
                                                    <FontAwesomeIcon icon={faFacebookF} />
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    <FontAwesomeIcon icon={faInstagram} />
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    <FontAwesomeIcon icon={faLinkedinIn} />
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                    
                                        <div className="_legals">
                                            <span className="_color _black _12px _fw700 d-block">
                                                Scamb Intermediação de Negócios  LTDA CNPJ: 12.345.678/0001 -09    
                                            </span>

                                            <span className="_color _black _12px _fw700 d-block">
                                                <a href="mailto:contato@scamb.com.vc" className="d-block">contato@scamb.com.vc</a> atendimento: seg a sex das 9h as 18h    
                                            </span>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>{/* END ./ Cols */}
                            
                        </div>{/* END ./ Container */}
                    </div>
                    
                    <div className="copyrights">
                        
                        <div className="_med-container">
                            <p className="_color _black _11px ">Scamb © 2020 - todos os direitos reservados</p>
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>
            
        )
        
    }
    
}

export default Footer;