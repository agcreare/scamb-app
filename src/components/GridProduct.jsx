import React, { Component } from  'react';
import { Link } from  'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


// Icons
import { faHeart } from '@fortawesome/pro-light-svg-icons';

class GridProduct extends Component {

    
    render() {
        
        return (
            
            <div className="_ui-item-wrapper">
                <Link to={this.props.link} className="_ui-item">
                    <div className="_ui-item-image">
                        <img src={this.props.image} className="_ui-item-image_block" alt="" />
                    </div>
                    <div className="_ui-item-bottom">
                        <div className="_col-left">
                            <div className="_product-title">
                                {this.props.title}
                            </div>
                            <div className="_product-price _1x">
                                <div className="ui-item__discount-price _is-hidden">
                                    <span class="price-tag-fraction">{this.props.discount}</span>
                                    <span class="price-tag-symbol">pts</span>
                                </div>
                                <div className="ui-item__price">
                                    <span class="price-tag-fraction">{this.props.price}</span>
                                    <span class="price-tag-symbol">pts</span>
                                </div>

                            </div>
                        </div>
                        <div className="_col-right">
                            <button type="button" className="btn btn-transparent _disabled">
                                <FontAwesomeIcon icon={faHeart}></FontAwesomeIcon>
                            </button>
                        </div>
                    </div>
                </Link>
            </div>
            
        );
        
    };
    
};

export default GridProduct;