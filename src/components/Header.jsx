import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import FixedMenu                        	from '../components/FixedMenu';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import {
        faInbox,
        faQuestionCircle,
        faSearch,
        faChevronDown,
        faEye,
        faHeart,
        faStore,
        faShoppingCart,
        faWallet,
        faCog,
        faSignOutAlt,
        faMegaphone
       } from '@fortawesome/pro-light-svg-icons';

// Images
import Logo from '../assets/images/logo.png';
import Avatar from '../assets/images/avatar.png';

class Header extends Component {

    constructor (props) {
        super(props);
        this.child = React.createRef();
    }

    onclick = () => {
        this.child.current.getAlert();
    }
    
    componentDidMount() {

        // Adicionar classe abaixo de 768
        window.addEventListener('resize', () => {
            this.setState({
                isMobile: window.innerWidth > 768
            });
        }, false);
        

        let categoryBtn     = $('.categories-menu');
        let userMenuBtn     = $('._user-menu-btn');
        let categoryMenu    = $('._categories-menu-dropdown');
        let userMenu        = $('._user-menu-dropdown');
        let mobileNavBtn    = $('._mobile-nav-btn');
        let leftMenu        = $('._aside-menu-categories');
        let mobileNavUser 	= $('._mobile-nav-user');
        let rightMenu		= $('._aside-menu-user');
        let closeNav        = $('.aside-menu > .aside-menu-header > #close');
        
        // Hover Category Menu
        $(categoryBtn).on('mouseover', function ()
        {
            $(categoryMenu).removeClass('_is-hidden');
        })
        .on('mouseout', function ()
        {
            $(categoryMenu).addClass('_is-hidden');
        });
        
        // Hover User Menu
        $(userMenuBtn).on('mouseover', function ()
        {
            $(userMenu).removeClass('_is-hidden');
        })
        .on('mouseout', function ()
        {
            $(userMenu).addClass('_is-hidden');
        });
        
        $(mobileNavBtn).on('click', function () {
            $(leftMenu).toggleClass('_is-hidden');
            $(rightMenu).addClass('_is-hidden');
        });
        
		$(mobileNavUser).on('click', function () {
            $(rightMenu).toggleClass('_is-hidden');
            $(leftMenu).addClass('_is-hidden');
        });
        
        $(closeNav).on('click', function () {
            $(leftMenu).addClass('_is-hidden');
            $(rightMenu).addClass('_is-hidden');
            console.log(this);
        });

        $('._list-nav._user > ul > li > a').on('click', function () {
            $(leftMenu).addClass('_is-hidden');
            $(rightMenu).addClass('_is-hidden');
        });

        $('.aside-nav > ul > li > a').on('click', function () {
            $(leftMenu).addClass('_is-hidden');
            $(rightMenu).addClass('_is-hidden');
        });
    }
    
    render () {
        
        return (
            
            <div className="_wrap-header">

                <FixedMenu></FixedMenu>    
            
            <div className="_header">
                
                {/* Container */}
                <div className="_med-container">
                    
                    {/* Cols */}
                    <div className="cols">

                        {/* Col left */}
                        <div className="col-left">

                            {/* Mobile menu icon */}
                            <div className="icon-nav-mobile">
                                <button type="button" className="_mobile-nav-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>{/* END ./ Mobile menu icon */}

                            {/* Logo */}
                            <div className="logo">
                                <Link to="./">
                                    <img src={Logo} alt="Scamb" />
                                </Link>
                            </div>{/* END ./ Logo */}
                            
                            {/* How it works */}
                            <div className="it-works-menu">
                                <button type="button">
                                    <FontAwesomeIcon icon={faQuestionCircle} className="fa-question-circle"/>
                                </button>
                            </div>{/* END ./ How it works */}

                            <div className="_my-points">
                                Meus pontos
                                <div className="_product-price _1x">
                                    <div className="ui-item__price">
                                        <span class="price-tag-fraction">1500</span>
                                        <span class="price-tag-symbol">pts</span>
                                    </div>

                                </div>
                            </div>

                        </div>{/* END ./ Col left */}

                        {/* Col right */}
                        <div className="col-right">

                            {/* Search form */}
                            <div className="search-menu">
                                
                                <form className="form">
                                    <div className={"form-group search " + this.state.isMobile ? 'form-group search mobile' : ''}>
                                        <input type="text" placeholder="Busque por 'Blusas'" />
                                        <button type="submit">
                                            <FontAwesomeIcon icon={faSearch} className="fa-question-circle"/>
                                        </button>
                                    </div>
                                </form>
                                
                            </div>{/* END ./ Search form */}
                            
                            <div className="categories-menu">
                                <button type="button" className="btn btn-transparent _color _black _fw700 _categories-btn">
                                    categorias <FontAwesomeIcon icon={faChevronDown} />
                                </button>
                                
                                <div className="_dropdown-menu _categories-menu-dropdown _positions _is-hidden">
                                    <nav className="_list-nav">
                                        <ul>
                                            <li>
                                                <Link to="/">
                                                    Roupas e etc
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    Kids
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    Beleza e saúde
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    Casa e decoração
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    Outros
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            
                            <div className="messages-menu">
                                
                                <Link to="./mensagens" className="btn btn-transparent _color _black">
                                    <span className="message-box">
                                        <FontAwesomeIcon icon={faInbox} />
                                        <span className="_notification-icon-mark _red"></span>
                                    </span>
                                </Link>
                                
                            </div>
                            
                            <div className="user-menu _user-menu-btn">
                                <button type="button" className="btn btn-transparent _color _black">
                                    <span className="user-image _rounded-image__with-notification-icon _1x">
                                        <img src={Avatar} alt="Avatar" />
                                        <span className="_notification-icon-mark _red"></span>
                                    </span>
                                </button>
                                
                                <div className="_dropdown-menu _user-menu-dropdown _is-hidden">
                                    <nav className="_list-nav">
                                        <ul>
                                            <li>
                                                <Link to="./minha-loja">
                                                    <span><FontAwesomeIcon icon={faEye} /></span>
                                                    Ver loja
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./meus-favoritos">
                                                    <span><FontAwesomeIcon icon={faHeart} /></span>
                                                    Meus Favoritos
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./minhas-vendas">
                                                    <span><FontAwesomeIcon icon={faStore} /></span>
                                                    Minhas Vendas
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./meus-anuncios">
                                                    <span><FontAwesomeIcon icon={faMegaphone} /></span>
                                                    Meus Anúncios
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./minhas-compras">
                                                    <span><FontAwesomeIcon icon={faShoppingCart} /></span>
                                                    Minhas Compras
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./minha-carteira">
                                                    <span><FontAwesomeIcon icon={faWallet} /></span>
                                                    Carteira
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./meu-perfil">
                                                    <span><FontAwesomeIcon icon={faCog} /></span>
                                                    Configurações
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./">
                                                    <span><FontAwesomeIcon icon={faQuestionCircle} /></span>
                                                    Ajuda
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="./">
                                                    <span><FontAwesomeIcon icon={faSignOutAlt} /></span>
                                                    Sair
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            
                            <div className="it-works-menu _is-hidden">
                                <FontAwesomeIcon icon={faQuestionCircle} className="fa-question-circle"/>
                                <Link to="/como-funciona" className="btn btn-transparent _fw700 _color _black">como <br/> funciona</Link>
                            </div>

                            <div className="_my-points">
                                Meus pontos
                                <div className="_product-price _1x">
                                    <div className="ui-item__price">
                                        <span class="price-tag-fraction">1500</span>
                                        <span class="price-tag-symbol">pts</span>
                                    </div>

                                </div>
                            </div>
                            
                            <div className="announce-button">
                                <Link to="./criar-novo" className="btn btn-red _fw700">Quero vender</Link>
                            </div>
                            
                        </div>{/* END ./ Col right */}

                    </div>{/* END ./ Cols */}
                </div>{/* END ./ Container */}
                
            </div>
            </div>
        )
        
    }
    
}

export default Header;