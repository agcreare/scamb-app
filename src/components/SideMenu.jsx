import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Avatar from '../assets/images/avatar.png';

// Icons
import {
        faQuestionCircle,
        faEye,
        faHeart,
        faStore,
        faShoppingCart,
        faWallet,
        faCog,
        faSignOutAlt,
        faLongArrowRight,
		faMegaphone,
       } from '@fortawesome/pro-light-svg-icons';

class SideMenu extends Component {
    
    render () {
        
        return (
            
            <div id="aside" className={"aside-menu _rightwards "+ this.props.className}>
                
                <div className="aside-menu-header _user">
                    
                    <button type="button" id="close" className="btn close-user-menu">
                        <FontAwesomeIcon icon={faLongArrowRight} />
                    </button>
                    
                    <div className="user-informations">
                        <button type="button" className="btn btn-transparent _color _black">
                            <span className="user-image _rounded-image__with-notification-icon _2x">
                                <img src={Avatar} alt="Avatar" />
                            </span>
                            <div className="user-informations-name__and-email">
                                Emilia Clarke
                                <span>emiliaclarke@gmail.com</span>
                            </div>
                        </button>
                    </div>
                </div>
                
                <div className="button-to-announce">
                    <button type="button" className="btn btn-danger">
                        Quero vender
                    </button>
                </div>
                
                <nav className="_list-nav _user">
                    <ul>
                        <li>
                            <Link to="/minha-loja">
                                <span><FontAwesomeIcon icon={faEye} /></span>
                                Ver loja
                            </Link>
                        </li>
                        <li>
                            <Link to="/meus-favoritos">
                                <span><FontAwesomeIcon icon={faHeart} /></span>
                                Meus favoritos
                            </Link>
                        </li>
                        <li>
                            <Link to="/minhas-vendas">
                                <span><FontAwesomeIcon icon={faStore} /></span>
                                Minhas vendas
                            </Link>
                            
                            <span className="tagged">978</span>
                        </li>
						<li>
							<Link to="/meus-anuncios">
								<span><FontAwesomeIcon icon={faMegaphone} /></span>
								Meus Anúncios
							</Link>
						</li>
                        <li>
                            <Link to="/minhas-compras">
                                <span><FontAwesomeIcon icon={faShoppingCart} /></span>
                                Minhas compras
                            </Link>
                        </li>
                        <li>
                            <Link to="/minha-carteira">
                                <span><FontAwesomeIcon icon={faWallet} /></span>
                                Carteira
                            </Link>
                        </li>
                        <li>
                            <Link to="/meu-perfil">
                                <span><FontAwesomeIcon icon={faCog} /></span>
                                Configurações
                            </Link>
                        </li>
                        <li>
                            <Link to="/ajuda">
                                <span><FontAwesomeIcon icon={faQuestionCircle} /></span>
                                Ajuda
                            </Link>
                        </li>
                        <li>
                            <Link to="/login">
                                <span><FontAwesomeIcon icon={faSignOutAlt} /></span>
                                Sair
                            </Link>
                        </li>
                    </ul>
                </nav>
            </div>
              
        );
        
    };
    
};

export default SideMenu;