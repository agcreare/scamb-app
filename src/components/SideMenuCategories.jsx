import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faTimes, faChevronRight } from '@fortawesome/pro-light-svg-icons';

class SideMenuCategories extends Component {
    
    render () {
        
        return (
            
            <div id="aside" className={"aside-menu _leftwards "+this.props.className}>
                
                <div className="aside-menu-header">
                    Categorias
                    <button type="button" id="close" className="btn">
                        <FontAwesomeIcon icon={faTimes} />
                    </button>
                </div>
                
                <nav className="aside-nav">
                    <ul>
                        <li>
                            <Link to="/">Roupas e etc <FontAwesomeIcon icon={faChevronRight} /></Link>
                        </li>
                        <li>
                            <Link to="/">Kids <FontAwesomeIcon icon={faChevronRight} /></Link>
                        </li>
                        <li>
                            <Link to="/">Beleza e saúde <FontAwesomeIcon icon={faChevronRight} /></Link>
                        </li>
                        <li>    
                            <Link to="/">Casa e decoração <FontAwesomeIcon icon={faChevronRight} /></Link>
                        </li>
                        <li>    
                            <Link to="/">Outros <FontAwesomeIcon icon={faChevronRight} /></Link>
                        </li>
                    </ul>
                </nav>
            </div>
              
        );
        
    };
    
};

export default SideMenuCategories;