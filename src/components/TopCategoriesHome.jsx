import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import TopCategoryIMG1 from '../assets/images/top-category-1.png';
import TopCategoryIMG2 from '../assets/images/top-category-2.png';
import TopCategoryIMG3 from '../assets/images/top-category-3.png';
import TopCategoryIMG4 from '../assets/images/top-category-4.png';
import TopCategoryIMG5 from '../assets/images/top-category-5.png';

const top_categories = [
  
    { id: '_id-category-1', src: TopCategoryIMG1, description: 'renove tudo!', category: 'Roupas e etc..', color: '_red' },
    { id: '_id-category-2', src: TopCategoryIMG2, description: 'tudo para seu lar', category: 'Casa e decoração', color: '_orange' },
    { id: '_id-category-3', src: TopCategoryIMG3, description: 'De tudo', category: 'Kids', color: '_blue' },
    { id: '_id-category-4', src: TopCategoryIMG4, description: 'Se cuidar faz bem!', category: 'Beleza e saúde', color: '_violet' },
    { id: '_id-category-5', src: TopCategoryIMG5, description: 'Um pouco mais', category: 'Outros', color: '_blue-light' }
    
];

class TopCategories extends Component {
    
    render() {
        
        return (
        
            <div id="top-categories">
                
                {top_categories.map(({id, src, description, category, color}) =>
                    
                    <div className="_grid" key={id}>
                        
                        <Link id={id} to="/" className={"_grid-col _bg-color " + color}>
                            <div className="_grid-col-head">
                                <span className="_color _white">{description}</span>
                                <h1  className="_color _white">{category}</h1>
                            </div>

                            <img src={src} alt={description} />
                        </Link>
                                        
                    </div>
                                    
                )}
            </div>
            
            
            
            
        )
        
    };
    
};

export default TopCategories;