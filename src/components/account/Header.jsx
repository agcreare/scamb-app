import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faCloudUpload, faCameraAlt } from '@fortawesome/pro-light-svg-icons';

//Images
import CoverIMG from '../../assets/images/cover.png';
import Avatar from '../../assets/images/avatar.png';

class Header extends Component {
	
	render() {
		
		return (
			
			<div id="view_my-account-profile--header">
				
				<section className="_hero">
					<div className="_profile-cover" style={{backgroundImage: 'url('+CoverIMG+')'}}>
					
						<input type="file" accept="image/x-png, image/jpeg" name="profilePhotoCover" id="profile-photo-cover" className="_hidden" />
						<label htmlFor="profile-photo-cover" className="btn btn-transparent _color _black">
							<FontAwesomeIcon icon={faCameraAlt}></FontAwesomeIcon>
						</label>
							
					</div>
					<div className="_profile-bottom">
						<div className="_wrap-user">
							<div className="_wrap-user-image">
								<span className="user-image _rounded-image__with-notification-icon _3x">
									<img src={Avatar} alt="Avatar" />
								</span>

								<input type="file" accept="image/x-png, image/jpeg" name="profilePhoto" id="profile-photo" className="_hidden" />
								<label htmlFor="profile-photo" className="btn btn-transparent _color _black">
									<FontAwesomeIcon icon={faCloudUpload}></FontAwesomeIcon>
								</label>
							</div>
							
							<div className="_user-info">
								<h1 className="_user-name">
									Emilia clarke
									<span className="_user-location">
										Curitiba - PR
									</span>
								</h1>
							</div>
						</div>
						
						<div className="_profile-button-follow">
							<button type="button" className="btn btn-follow">
								Seguir
							</button>
						</div>
					</div>
				</section>
				
			</div>
			
		);
		
	};
	
};

export default Header;