import React, { Component } from 'react';
import Header from './Header';
import Menu from './Menu';
import { faClone } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import smallBanner from '../../assets/images/small-banner.png';

class Layout extends Component {
	
	render() {
		
		return (
			
			<div id="view_my-account-layout" className={this.props.className}>
				
				<Header></Header>

				<div className="_wrap-content">
					<div className="_cols">
						<div className="_col _col-left">
							<Menu></Menu>
						</div>
						<div className="_col _col-right">
							<section className="_banner-fixed" style={{backgroundImage: `url(${smallBanner})`}}>

							<div className="_texts-banner">
								<h1 className="_the-title _color _white _fw700">Convide amigos e ganhe pontos</h1>
								<span className="_subtitle _color _white _fw700">Copie seu link e envie para seus amigos</span>
							</div>

							<div className="_invite-label">
								<form className="_form">
									<div className="form-group">
										<input type="text" className="form-control" placeholder="https://scamb.com.vc/x45dcz" value="https://scamb.com.vc/x45dcz" disabled="disabled" />
										<button type="button" className="btn">
											<FontAwesomeIcon icon={faClone}></FontAwesomeIcon>
										</button>
									</div>
								</form>
							</div>

							</section>
						<div className="_wrap-col-content">
							{this.props.children}
						</div>
						</div>
					</div>
				</div>
					
			</div>
			
		);
		
	};
	
};

export default Layout;