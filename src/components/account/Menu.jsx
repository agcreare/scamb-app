import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import {faChevronRight,
        faEye,
        faHeart,
        faStore,
        faShoppingCart,
        faWallet,
        faCog,
		faMegaphone } from '@fortawesome/pro-light-svg-icons';

class Menu extends Component {
	
	render() {
		
		return (
			
			<div id="view_my-account-menu">
				
				<div className="_menu">
					<nav className="_list-nav">
						<ul>
							<li>
								<Link to="/minha-loja">
									<span><FontAwesomeIcon icon={faEye} /></span>
									Ver loja
								</Link>
								<FontAwesomeIcon icon={faChevronRight} className="chevron-right" />
							</li>
							<li>
								<Link to="/meus-anuncios">
									<span><FontAwesomeIcon icon={faMegaphone} /></span>
									Meus Anúncios
								</Link>
								<FontAwesomeIcon icon={faChevronRight} className="chevron-right" />
							</li>
							<li>
								<Link to="/minhas-compras">
									<span><FontAwesomeIcon icon={faShoppingCart} /></span>
									Minhas Compras
								</Link>
								<FontAwesomeIcon icon={faChevronRight} className="chevron-right" />
							</li>
							<li>
								<Link to="/minhas-vendas">
									<span><FontAwesomeIcon icon={faStore} /></span>
									Minhas Vendas
								</Link>
								<FontAwesomeIcon icon={faChevronRight} className="chevron-right" />
							</li>
							<li>
								<Link to="/meus-favoritos">
									<span><FontAwesomeIcon icon={faHeart} /></span>
									Meus Favoritos
								</Link>
								<FontAwesomeIcon icon={faChevronRight} className="chevron-right" />
							</li>
							<li>
								<Link to="/minha-carteira">
									<span><FontAwesomeIcon icon={faWallet} /></span>
									Carteira
								</Link>
								<FontAwesomeIcon icon={faChevronRight} className="chevron-right" />
							</li>
							<li>
								<Link to="/meu-perfil">
									<span><FontAwesomeIcon icon={faCog} /></span>
									Configurações
								</Link>
								<FontAwesomeIcon icon={faChevronRight} className="chevron-right" />
							</li>
						</ul>
					</nav>
				</div>
				
			</div>
			
		);
		
	};
	
};

export default Menu;