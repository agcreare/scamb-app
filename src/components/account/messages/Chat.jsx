import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Scrollbars } from "react-custom-scrollbars";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import $ from "jquery";

// Icons
import {
  faChevronLeft,
  faCog,
  faPaperPlane,
  faImage,
  faPaperclip,
  faThumbsUp,
  faThumbsDown,
} from "@fortawesome/pro-light-svg-icons";

// Images
import Avatar from "../../../assets/images/user-proposal.png";
import ProductProposalIMG from "../../../assets/images/product-single-img-1.png";

class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
	  chatMessenger: false,
	  dropdown: false
    };
  }

  closeMessenger = () => {
    this.setState({
      chatMessenger: false,
    });
  };

  showMessenger = () => {
    this.setState({
      chatMessenger: true,
    });
  };

  handleDDown = () => {
    this.setState({
      dropdown: !this.state.dropdown,
    });
  };

  render() {
    return (
      <>
        {this.state.chatMessenger ? (
          <section className={this.props.className}>
            <form className="_chat-form">
              <section className="_chat-header">
                <div className="_cols">
                  <div className="_col _col-left">
                    <div className="_back">
                      <button
                        type="button"
                        className="btn btn-transparent _disabled"
                        onClick={this.closeMessenger}
                      >
                        <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>
                      </button>
                    </div>

                    <div className="_current-user">
                      <span className="user-image _rounded-image__with-notification-icon _1x">
                        <img src={Avatar} alt="Avatar" />
                      </span>
                      <span className="username">Marina Hertzz</span>
                    </div>
                  </div>

                  <div className="_col _col-right">
                    <div className="_chat-config">
                      <button
                        type="button"
						className="btn btn-transparent _disabled _options"
						onClick={this.handleDDown}
                      >
                        <FontAwesomeIcon icon={faCog}></FontAwesomeIcon>
                      </button>

						{this.state.dropdown ? (
                      <div
                        className="_dropdown-menu"
                        id="chat-options"
                      >
                        <nav className="_list-nav">
                          <ul>
                            <li>
                              <Link to="/produto">Ver meu anúncio</Link>
                            </li>
                            <li>
                              <Link to="/">Excluir conversa</Link>
                            </li>
                            <li>
                              <Link to="/ajuda">Denunciar usuário</Link>
                            </li>
                          </ul>
                        </nav>
                      </div>
					  ) : null}

                    </div>
                  </div>
                </div>
              </section>

              <section className="_chat-product">
                <div className="_cols">
                  <div className="_col _col-left">
                    <div className="_product">
                      <div className="product-image">
                        <img src={ProductProposalIMG} alt="" />
                      </div>
                      <div className="_product-info">
                        <div className="_product-tag">
                          <span className="_tag proposal">proposta</span>
                          de
                          <span className="_tag-username">Marina Hertz</span>
                        </div>

                        <div className="_product-item">
                          em{" "}
                          <span className="_product-item-tag">
                            short listrado jeans
                          </span>
                        </div>

                        <div className="_product-price _1x">
                          <div className="ui-item__price">
                            <span className="price-tag-fraction">68</span>
                            <span className="price-tag-symbol">pts</span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="_wrap-actions">
                      <div className="_proposal-value">
					  <span className="hide-this">Valor da</span> proposta
                        <div className="_product-price _1x">
                          <div className="ui-item__price">
                            <span className="price-tag-fraction">55</span>
                            <span className="price-tag-symbol">pts</span>
                          </div>
                        </div>
                      </div>

                      <div className="_actions">
                        <button type="submit" className="btn decline">
                          <FontAwesomeIcon
                            icon={faThumbsDown}
                          ></FontAwesomeIcon>
                        </button>
                        <button type="submit" className="btn accept">
                          <FontAwesomeIcon icon={faThumbsUp}></FontAwesomeIcon>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section className="_chat-content">

                <div className="_chat-messages">
                  <div className="message-date">21 DE MAIO DE 2020 17:58</div>

                  <div className="_wrap-received">
                    <div className="_from-user">
                      <span className="user-image _rounded-image__with-notification-icon _1x">
                        <img src={Avatar} alt="Avatar" />
                      </span>
                    </div>

                    <div className="_received">
                      <div className="_who-sent">Marina Hertz</div>
                      <div className="_wrap-chat-messages">
                        <div className="chat-message-bubble received">
                          Oi, tudo bem?
                        </div>
                        <div className="chat-message-bubble received">
                          Voce aceita 55 pontos no shorts?
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="_wrap-sent">
                    <div className="_sent">
                      <div className="_wrap-chat-messages">
                        <div className="chat-message-bubble sent">Oi</div>
                        <div className="chat-message-bubble sent">
                          ainda não vendi
                        </div>
                        <div className="chat-message-bubble sent">
                          tem interesse?
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="_wrap-received">
                    <div className="_from-user">
                      <span className="user-image _rounded-image__with-notification-icon _1x">
                        <img src={Avatar} alt="Avatar" />
                      </span>
                    </div>

                    <div className="_received">
                      <div className="_who-sent">Marina Hertz</div>
                      <div className="_wrap-chat-messages">
                        <div className="chat-message-bubble received">
                          Oi, tudo bem?
                        </div>
                        <div className="chat-message-bubble received">
                          Voce aceita 450 pontos na bolsa?
                        </div>
                        <div className="chat-message-bubble received">
                          ainda tem a bolsa?
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section className="_chat-footer">
                <section className="_chat-send">
                  <div className="_wrap-send">
                    <div className="send-text">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Digite uma mensagem..."
                      />
                    </div>
                    <div className="send-message">
                      <button
                        type="button"
                        className="btn btn-transparent _disabled"
                      >
                        <FontAwesomeIcon icon={faPaperPlane}></FontAwesomeIcon>
                      </button>
                    </div>
                  </div>
                </section>

                <section className="_chat-attachments">
                  <div className="_wrap-attachments">
                    <div className="attachment-file">
                      <label
                        htmlFor="uploadPhoto"
                        className="attachment"
                        title="Anexar uma foto"
                      >
                        <input
                          type="file"
                          accept="image/x-png, image/jpeg"
                          name="upload_photo"
                          id="uploadPhoto"
                          multiple="multiple"
                          className="form-control-file"
                        />
                        <button type="button" className="btn btn-transparent">
                          <FontAwesomeIcon icon={faImage}></FontAwesomeIcon>
                        </button>
                      </label>
                    </div>
                    <div className="attachment-file">
                      <label
                        htmlFor="uploadFile"
                        className="attachment"
                        title="Anexar um arquivo"
                      >
                        <input
                          type="file"
                          accept="image/x-png, image/jpeg"
                          name="upload_photo"
                          id="uploadFile"
                          multiple="multiple"
                          className="form-control-file"
                        />
                        <button type="button" className="btn btn-transparent">
                          <FontAwesomeIcon icon={faPaperclip}></FontAwesomeIcon>
                        </button>
                      </label>
                    </div>
                  </div>
                </section>
              </section>
            </form>
          </section>
        ) : null}
      </>
    );
  }
}

export default Chat;
