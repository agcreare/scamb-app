import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faTimes, faCheck } from '@fortawesome/pro-light-svg-icons';

class Messages extends Component {
	
	
	
	render() {
		
		let action_status;
		let message_action = this.props.action;
		let message_date = this.props.message_date;
		let message_username = this.props.username;
		let message_product_title = this.props.product_title;
		
		function markup(action_text, status_text, message_text, classTag) {
			
			return (
				<div className="_content">
					<div className="_message-status">
						<span className={"_tag "+classTag}>{action_text}</span>
						{status_text}
					</div>
					<div className="_message-date">
						{message_date}
					</div>
					
					<div className="_message-text">
						<span className="_message-username">{message_username}:</span>
						<span className="_the-message">{message_text}:</span>
						<span className="_message-product-title">{message_product_title}</span>
					</div>
					
				</div>
			)
		}
		
		switch(message_action) {
			case 'proposal':
				action_status = markup('proposta','para avaliar','Fez uma proposta em', 'proposal');
				break;
			case 'doubt':
				action_status = markup('dúvida', 'Para responder','Tem uma dúvida sobre', 'doubt');
				break;
				break;
			case 'purchase':
				action_status = markup('compra', 'parabéns!','Comprou seu item', 'purchase');
				break;
		}
		
		return (
			
			<div className="_wrap-messages-items">
				
				<div className="_wrap-message-item">

					{/*  Switch wrapper <DIV> to <LINK>  */}
					<div className={"_message-item "+ this.props.status} onClick={this.props.onClick}>
						<div className="_message-image">
							<span className="user-image _rounded-image__with-notification-icon _2x">
								<div className="_wrap-image">
									<img src={this.props.image} alt="Avatar" />
								</div>
								<span className="_notification-icon-mark _blue"></span>
							</span>
						</div>

						<div className="_message-content">
							{action_status}
						</div>
					</div>

					<div className="_action">
						<div className="_checkbox form-check">
							<label className="form-check-label">

								<input type="checkbox" className="form-check-input" />
								<span className="check">
								<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
								</span>

							</label>
						</div>
					</div>

				</div>
			</div>
			
		);
		
	};
	
};

export default Messages;