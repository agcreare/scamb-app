import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import { faTimes, faCheck } from '@fortawesome/pro-light-svg-icons';

class News extends Component {
	
	componentDidMount() {
		
		$("div#news_text").text(function(index, currentText) {
			return currentText.substr(0, 50) + '...';
		});
		
	}	
	
	render() {
		
		let news_status = this.props.status;
		let news_image = this.props.image;
		let news_date = this.props.news_date;
		let news_title = this.props.news_title;
		let news_text = this.props.news_text;
		
		return (
			
			<div>
				
				<div className="_wrap-messages-items">
				<div className="_wrap-message-item">
				
				<Link to="/" className={"_message-item "+ this.props.status}>
					<div className="_message-image">
						<span className="user-image _rounded-image__with-notification-icon _2x">
							<img src={news_image} alt="Avatar" />
							<span className="_notification-icon-mark _blue"></span>
						</span>
					</div>

					<div className="_message-content">
						<div className="_content">
							<div className="_message-text _news">
								
								<div className="_message-title">
									{news_title}
								</div>

								<div className="_message-date">
									{news_date}
								</div>

								<div className="_the-message" id="news_text">
									{news_text}
								</div>

							</div>
						</div>
					</div>
				</Link>
				
				<div className="_action">
					<div className="_checkbox form-check">
						<label className="form-check-label">

							<input type="checkbox" className="form-check-input" />
							<span className="check">
							<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
							</span>

						</label>
					</div>
				</div>
				
			</div>
			</div>
				
			</div>
			
		);
		
	};
	
};

export default News;