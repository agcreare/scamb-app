import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Accordion, Card, Button} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faChevronDown, faCreditCardBlank, faUser, faCalendarStar, faCalendarAlt, faLockAlt } from '@fortawesome/pro-light-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/pro-solid-svg-icons';

// Images
import Visa from '../../../assets/images/credit-card/visa.png';
import Mastercard from '../../../assets/images/credit-card/mastercard.png';
import AmericanExpress from '../../../assets/images/credit-card/american-express.png';
import DinersClub from '../../../assets/images/credit-card/diners-club.png';

class CreditCard extends Component {
    
    render() {
        
        return (
            
            <Accordion className="_add-payment-accordion _accordion">
              <Card>
                <Card.Header>
                  <Accordion.Toggle as={Button} variant="link" eventKey="0">

                    <div className="_wrap-current-cc">
                        
                        <div className="_current-cc-info">
                            <div className="_cc-flag">
                                <div className="_cc-flag-icon">
                                    <img src={Mastercard}  alt="" />
                                </div>
                                <span className="_tag">principal</span>
                            </div>

                            <div className="_cc-info">
                                <div className="_cc-number-prev">
                                    Mastercard (<span className="_cc-number">**** 3501</span>)
                                </div>
                                <div className="_cc-exp-date">
                                    Expira em: 07/2027
                                </div>
                                <div className="_cc-name">
                                    Emilia Clarke
                                </div>
                            </div>
                        </div>
                    
                        {<button type="button" className="btn _delete-card">Excluir cartão</button>}
                        
                    </div>
                    
                    <button type="button" className="btn btn-transparent toggle-arrow">
                        <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>  
                    </button>  
                      
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="0">
                    <Card.Body>
                        
                        <section className="_update-cc">
                            <section className="_update-cc-informations">
                                
                                <h1 className="_update-cc-title">
                                    Atualizar informações do cartão de crédito
                                </h1>
                                
                                <ul className="_payment-methods-list">
                                    <li>
                                        <div className="creadit-card--flag">
                                            <img src={Visa}  alt="" />
                                        </div>
                                    </li>
                                    <li>
                                        <div className="creadit-card--flag">
                                            <img src={Mastercard}  alt="" />
                                        </div>
                                    </li>
                                    <li>
                                        <div className="creadit-card--flag">
                                            <img src={AmericanExpress}  alt="" />
                                        </div>
                                    </li>
                                    <li>
                                        <div className="creadit-card--flag">
                                            <img src={DinersClub}  alt="" />
                                        </div>
                                    </li>
                                </ul>
                                
                                <form className="_form">
                                    
                                    <div class="input-group mb-3">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text" id="cc-number">
                                            <FontAwesomeIcon icon={faCreditCardBlank}></FontAwesomeIcon>    
                                        </span>
                                      </div>
                                      <input type="text" class="form-control" placeholder="Número do cartão" aria-describedby="cc-number" />
                                    </div>
                                    
                                    <div class="input-group mb-3">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text" id="cc-name">
                                            <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>    
                                        </span>
                                      </div>
                                      <input type="text" class="form-control" placeholder="Nome titular do cartão" aria-describedby="cc-name" />
                                    </div>
                                    
                                    <div className="_row">
                                        
                                        <div class="input-group mb-3">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text" id="cc-month">
                                                <FontAwesomeIcon icon={faCalendarStar}></FontAwesomeIcon>    
                                            </span>
                                          </div>
                                          <input type="text" class="form-control" placeholder="Mês" aria-describedby="cc-month" />
                                        </div>
                                        
                                        
                                        <div class="input-group mb-3">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text" id="cc-year">
                                                <FontAwesomeIcon icon={faCalendarAlt}></FontAwesomeIcon>    
                                            </span>
                                          </div>
                                          <input type="text" class="form-control" placeholder="Ano" aria-describedby="cc-year" />
                                        </div>
                                        
                                        
                                        <div class="input-group mb-3">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text" id="cc-ccv">
                                                <FontAwesomeIcon icon={faLockAlt}></FontAwesomeIcon>    
                                            </span>
                                          </div>
                                          <input type="text" class="form-control" placeholder="CVV" aria-describedby="cc-cvv" />
                                        </div>
                                        
                                    </div>
                                    
                                    <div className="form-group-button">
									   <button type="submit" className="btn btn-primary">Salvar</button>
								    </div>	
                                    
                                </form>
                            </section>
                            
                            <section className="_update-address">
                                <div className="_set-user-location">
                                    
                                    <h1 className="_user-location-title">
                                        Endereço de cobrança
                                    </h1>
                                    
                                    <div className="_wrap-location">

                                        <div className="_icon">
                                            <FontAwesomeIcon icon={faMapMarkerAlt}></FontAwesomeIcon>
                                        </div>

                                        <div className="_wrap-current-location">
                                            <div className="_location-title">
                                                CEP: 85050-030
                                            </div>
                                            <div className="_location">
                                                R Bartolomeu bueno da silva,
                                                n359 - Bairro Água Verde,
                                                Curitiba - Paraná
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <Link className="link-button" to="/">Alterar endereço de cobrança</Link>
                            </section>
                        </section>
                        
                    </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
            
            
        );
        
    };
    
};

export default CreditCard;