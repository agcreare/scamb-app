import React, {Component, useState} from 'react';

import {Button, Modal} from 'react-bootstrap';


class ModalBase extends Component {

    constructor() {
        super();
        this.state = {show: false};
    }

    setShow = (show) => {
        this.setState({show: show})
    }
     
    handleClose = () => {this.setShow(false)};
    handleShow = () => {this.setShow(true)};

    render () {
    
      return (
        <>
            <Modal show={this.state.show} onHide={this.handleClose} centered>
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                <div className="_title">
                    {this.props.modalTitle}
                    <span className="_subtitle">
						{this.props.modalSubTitle}
                    </span>
                </div>
                    {this.props.children}
                </Modal.Body>
            </Modal>
        </>
      );
    
}
}

export default ModalBase;