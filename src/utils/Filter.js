import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faLongArrowLeft, faChevronDown } from '@fortawesome/pro-light-svg-icons';

class Test extends Component {

    constructor(props) {
        super(props);
        this.addActiveClass = this.addActiveClass.bind(this);
        this.state = {
            active: false,
        };
    }
    toggleClass() {
        let current = null;
        const currentState = this.state.active;
        current.setState({ active: !currentState });
    };

    addActiveClass() {
      this.setState({
        isActive: true
      })
    }
    
    componentDidMount() {
        window.addEventListener("scroll", this.handeleScroll); // remove brackets ()
      }
    
    render() {
        
        
        return (
            <div className="_wrap-filter">
                <div className={this.state.active ? '_filter _active' : '_filter _inactive'}>

                    <button type="button" className="_filter-bar" onClick={ () => this.setState({active: !this.state.active}) }>
                        <Link className="btn btn-transparent" onClick={ () => this.setState({active: !this.state.active})}>
                            <FontAwesomeIcon className="arrow-left" icon={faLongArrowLeft}></FontAwesomeIcon>
                            <FontAwesomeIcon className="chevron-down" icon={faChevronDown}></FontAwesomeIcon>
                        </Link>
                        <div className="_filter-title">
                            {this.props.filterTitle}
                        </div>
                    </button>

                    <div className="_filter-label">
                        {this.props.filterTitle}
                    </div>

                    <div className="_filter-content">
                        <div className={"_form-filter-content "+ this.props.filterType}>
                            {this.props.children}
                        </div>
                        <div className="_form-filter-footer">
                            <button className="btn btn-primary">Aplicar</button>
                            <button className="btn btn-cancel">Limpar</button>
                        </div>
                    </div>

                    <p onClick={ () => this.setState({active: !this.state.active}) } >{this.props.text}</p>
                </div>
            </div>
        );
        
    };
    
};

export default Test;