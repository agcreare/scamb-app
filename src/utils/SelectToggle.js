import React from 'react';
import $ from 'jquery';
 
export const selectFx = () => {
    
    
    $(function () {
        var current = null;
        var upSpeed = 300;

        $("._dropdown-select").click(function() {

          $(this).children("._form-select-content").slideDown(upSpeed);

          if (current != this) {
                $(current).children("._form-select-content").slideUp(upSpeed);
                current = this;
          } else {
              current = null
          };

        });

        $("._select-content").on('click', function () {

            let fieldValue = $(this).find("._current-item-name").text();

            if (current != this) {
                $(this).closest('._dropdown-select').find("._the-label > ._span-name").text(fieldValue);
                current = this;
            } else current = null;

        });
    });
    
}