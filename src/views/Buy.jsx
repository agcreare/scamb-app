import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ModalBase from '../components/modals/ModalBase';
import ModalTaxs from './modals/ModalTaxs';
import ModalShippingOptions from './modals/ModalShippingOptions';

// Icons
import {
    faCheck,
    faQuestionCircle,
    faTruck,
    faMobile,
    faHandHoldingBox } from '@fortawesome/pro-light-svg-icons';
import {
    faMapMarkerAlt } from '@fortawesome/pro-solid-svg-icons';

// Images
import ProductIMG from '../assets/images/product-single-img-1.png';

class Buy extends Component {

    constructor(props) {
        super(props)
        this.childChangeAddress = React.createRef();
        this.childTaxs = React.createRef();
        this.childShippingOptions = React.createRef();
    }

    openModalChangeAddress = () => {
        this.childChangeAddress.current.handleShow();
    }

    closeModalChangeAddress = () => {
        this.childChangeAddress.current.handleClose();
    }

    openModalTaxs = () => {
		this.childTaxs.current.handleShow();
	}

    openModalShippingOptions = () => {
		this.childShippingOptions.current.handleShow();
	}
    
    render() {
        
        return (
            
            <section id="view_buy">
                <div className="_med-container">
                    
                    <div className="_wrap-product">
                        <div className="_product">
                            <img src={ProductIMG} alt="" />
                        </div>
                        
                        <div className="_product-info">
                            <div className="_product-title">
                            short listrado jeans
                            </div>
                            <div className="_product-price">
                                <div className="_product-price _1x">
                                    <div className="ui-item__price">
                                        <span class="price-tag-fraction">68</span>
                                        <span class="price-tag-symbol">pts</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <form className="_cols">
                        
                        <div className="_col _col-left">
                            <div className="box _form">
                                <div className="_box-title">
                                    Endereço de entrega
                                    <Link onClick={this.openModalChangeAddress}> editar endereço</Link>
                                </div>
                                
                                <div className="_box-content">
                                    <div className="_set-user-location">
                                    
                                        <div className="_wrap-location">

                                            <div className="_icon">
                                                <FontAwesomeIcon icon={faMapMarkerAlt}></FontAwesomeIcon>
                                            </div>

                                            <div className="_wrap-current-location">
                                                <div className="_location-title">
                                                    CEP: 85050-030
                                                </div>
                                                <div className="_location">
                                                    R Bartolomeu bueno da silva,
                                                    n359 - Bairro Água Verde,
                                                    Curitiba - Paraná
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div className="form-group _delivery-type">
									
                                        <label className="_label">Opções de entrega *
                                         <button type="button" className="btn btn-transparent _disabled" onClick={this.openModalShippingOptions}>
                                            <FontAwesomeIcon icon={faQuestionCircle}></FontAwesomeIcon>
                                        </button>
                                        <span className="for-label">Seleciona a melhor opção de frete para voce.</span>
                                      </label>

                                    <div className="_wrap-check-radio radio">

                                        <label className="_check-radio">
                                            <input type="radio" className="form-check-input" name="tipo_entrega" />
                                            <span className="overlay"></span>
                                            <div className="_wrap-alt-icon">
                                                <div className="_alt-icon">
                                                    <FontAwesomeIcon icon={faTruck}></FontAwesomeIcon>
                                                </div>
                                                Operador logístico	
                                            </div>

                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>

                                      </label>

                                        <label className="_check-radio">
                                            <input type="radio" className="form-check-input" name="tipo_entrega" />
                                            <span className="overlay"></span>
                                            <div className="_wrap-alt-icon">
                                                <div className="_alt-icon">
                                                    <FontAwesomeIcon icon={faMobile}></FontAwesomeIcon>
                                                </div>
                                                Aplicativo de entrega	
                                            </div>

                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>

                                      </label>

                                        <label className="_check-radio">
                                            <input type="radio" className="form-check-input" name="tipo_entrega" />
                                            <span className="overlay"></span>
                                            <div className="_wrap-alt-icon">
                                                <div className="_alt-icon">
                                                    <FontAwesomeIcon icon={faHandHoldingBox}></FontAwesomeIcon>
                                                </div>
                                                Entrega em mãos	
                                            </div>

                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>

                                      </label>

                                    </div>	
                                    </div>
                                    

                                    <div className="notice">
                                        Tempo estimado de engrega até 7 dias úteis.
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div className="_col _col-right">
                            <div className="box">
                                <div className="_box-title">
                                    Resumo do pedido
                                </div>
                                
                                <ul className="resume-list">
                                    <li>
                                        <div className="resume-list-title">Valor</div>
                                        <div className="resume-list-value">
                                            <div className="_title">short listrado jeans</div>
                                            <div className="_value">68 Pontos</div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="resume-list-title">Frete</div>
                                        <div className="resume-list-value">
                                            <div className="_title">Operador logístico</div>
                                            <div className="_value">R$ 9,90</div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="resume-list-title">
                                            Taxas
                                            <button type="button" className="btn btn-transparent _disabled" onClick={this.openModalTaxs}>
                                                <FontAwesomeIcon icon={faQuestionCircle}></FontAwesomeIcon>
                                            </button>
                                        </div>
                                        <div className="resume-list-value">
                                            <div className="_title">Taxas de serviço</div>
                                            <div className="_value">R$ 4,90</div>
                                        </div>
                                    </li>
                                    <li className="_wbg">
                                        <div className="resume-list-title">
                                            Total em pontos
                                        </div>
                                        <div className="resume-list-value">
                                            <div className="_title">Total a pagar em pontos</div>
                                            <div className="_value _price">68 Pontos</div>
                                        </div>
                                    </li>
                                    <li className="_wbg">
                                        <div className="resume-list-title">
                                            Total em dinheiro
                                        </div>
                                        <div className="resume-list-value">
                                            <div className="_title">Total a pagar em dinheiro</div>
                                            <div className="_value _price">R$14,80</div>
                                        </div>
                                    </li>
                                </ul>
                                
                                <div className="notice">
                                    Ao clicar em confirmar você aceita os termos e condições da plataforma.
                                </div>
                                
                                <div className="_buttons">
                                    <div className="form-group-button">
                                        <button type="submit" className="btn btn-success">Comprar</button>
                                        <button type="submit" className="btn btn-cancel">Cancelar</button>
								    </div>	
                                </div>
                                
                            </div>
                        </div>
                    
                    </form>
                    
                </div>

                <ModalBase ref={this.childChangeAddress} modalTitle="Alterar Endereço" modalSubTitle="Ao alterar este sera seu endereço de cobrança padrão">
                <form className="_form _change-addrress">

                    <div className="form-group">
                        <label className="_label">Cep</label>
                        <input type="text" className="form-control" placeholder="Informe seu cep" defaultValue="00000-00" />
                        <small className="form-text text-muted">Não sabe seu cep? <a rel="noopener noreferrer" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">clique aqui</a></small>
                    </div>

                    <div className="form-group">
                        <label className="_label">Estado</label>
                        <input type="email" className="form-control" placeholder="Qual seu estado?" defaultValue="Paraná" />
                    </div>

                    <div className="form-group">
                        <label className="_label">Cidade</label>
                        <input type="text" className="form-control" placeholder="Qual sua cidade?" defaultValue="Curitiba" />
                    </div>

                    <div className="form-group">
                        <label className="_label">Endereço</label>
                        <input type="text" className="form-control" placeholder="Seu CPF" defaultValue="R Bartolomeu bueno da silva" />
                    </div>

                    <div className="form-group">
                        <label className="_label">Número</label>
                        <input type="text" className="form-control" placeholder="Número" defaultValue="359" />
                    </div>

                    <div className="form-group">
                        <label className="_label">Complemento</label>
                        <input type="text" className="form-control" placeholder="..." defaultValue="" />
                    </div>

                    <div className="form-group">
                        <label className="_label">Bairro</label>
                        <input type="text" className="form-control" placeholder="Qual o bairro?" defaultValue="Bairro Água Verde" />
                    </div>

                    <div className="_form-footer">
                        <button type="submit" className="btn btn-red">Confirmar alterações</button>
                        <button type="button" className="btn btn-cancel" onClick={this.closeModalChangeAddress}>Cancelar</button>
                    </div>

                    </form>
                </ModalBase>

                <ModalBase ref={this.childShippingOptions} modalTitle="Opções de entrega" modalSubTitle="Qual a  maneira mais prática para vocês?">
                    <ModalShippingOptions></ModalShippingOptions>
                </ModalBase>

                {/* Modal Taxas Scamb */}
				<ModalBase ref={this.childTaxs} modalTitle="Taxas de administração" modalSubTitle="
					Voce não paga nada para listar produtos no Scamb e ter uma lojinha ativa. Porém, a cada transação
					efetivada, o Scamb cobra uma taxa de administração para apoiar a manutenção da plataforma. No
					momento da confirmação da transação, o valor é debitado automaticamente no cartão de crédito cadastrado.
				">

					<ModalTaxs></ModalTaxs>

				</ModalBase>

            </section>
            
        )
        
    }
    
}

export default Buy;