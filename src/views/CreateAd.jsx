import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import { faCheck, faTruck, faMobile, faHandHoldingBox, faTrashAlt, faChevronDown, faChevronRight } from '@fortawesome/pro-light-svg-icons';
import { faQuestionCircle, faCamera, faMapMarkerAlt } from '@fortawesome/pro-solid-svg-icons';

//Images
import ProductIMG from '../assets/images/product-single.png';

class CreateAd extends Component {
	
    componentDidMount() {
        
        var currentSelected = null;
        var current = null;
        var upSpeed = 300;
        
        $("._dropdown-select").click(function() {
            
          $(this).children("._form-select-content").slideDown(upSpeed);
        
          if (current != this) {
                $(current).children("._form-select-content").slideUp(upSpeed);
                current = this;
          } else {
              current = null
          };
            
        });
        
        $("._select-content").on('click', function () {
        
            let fieldValue = $(this).find("._current-item-name").text();
            
            if (currentSelected != this) {
                $(this).closest('._dropdown-select').find("._the-label > ._span-name").text(fieldValue);
                currentSelected = this;
            } else currentSelected = null;

        });
        
    }
    
	render () {
		
		return (
			
			<div id="view_create-ad">
				
				<div className="_wrap-create-ad">
					
					<div className="create-ad box box-shadow">
						
						<h1 className="_create-ad-title _color _black _fw700">
							O Que voce está anunciando?
							<span className="heading">
								O titulo e a descrição fazem a diferença no anúncio!
							</span>
						</h1>
						
						<div className="_wrap-form">
							
							<form className="_form">
								
							  <div className="form-group">
								<input type="text" className="form-control" placeholder="Nome completo" />
							  </div>
								
							  <div className="form-group">
								 <span className="_character-counter">5 de 350</span> 
								<textarea className="form-control" placeholder="Descrição" rows="3"></textarea>
							  </div>
                                
                                <div className="_wrap-form-select">
                                    
                                    <div className="_form-select _dropdown-select">
                                        <div className="_the-label">
                                            <span className="_span-name">Condição *</span>
                                            <span className="caret">
                                                <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>
                                            </span>
                                        </div>

                                        <div className="_form-select-content" style={{display: 'none'}}>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="condicao"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Novo</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="condicao"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Usado</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div className="_wrap-form-select">
                                    
                                    <div className="_form-select _dropdown-select">
                                        <div className="_the-label">
                                            <span className="_span-name">Marca *</span>
                                            <span className="caret">
                                                <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>
                                            </span>
                                        </div>

                                        <div className="_form-select-content" style={{display: 'none'}}>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="marca"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Morena rosa</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="marca"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Beagle</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="marca"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Denuncia</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div className="_wrap-form-select">
                                    
                                    <div className="_label">
                                        Categorias *
                                    </div>
                                    <div className="_form-select _dropdown-select">
                                        <div className="_the-label">
                                            <span className="_span-name">Categorias *</span>
                                            <span className="caret">
                                                <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>
                                            </span>
                                        </div>

                                        <div className="_form-select-content" style={{display: 'none'}}>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="categorias"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Roupas e etc</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="categorias"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Kids</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="categorias"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Casa e decoração</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="categorias"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Beleza e saúde</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                            <div className="_select-item">
                                                <label className="ui-checkbox">
                                                    <input type="radio" className="_checkbox" name="categorias"/>
                                                    <div className="_select-content">
                                                        <span className="_item-name _current-item-name">Outros</span>
                                                        <span className="_item-icon">
                                                            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
							  <div className="form-group">
								 <label htmlFor="price" className="_label">Preço em pontos * 
									 <button type="button" className="btn btn-transparent _disabled">
										<FontAwesomeIcon icon={faQuestionCircle}></FontAwesomeIcon>
									</button>
								  </label>
								<input type="text" id="price" className="form-control" placeholder="Digite o preço" />
							  </div>
								
								<div className="form-group _delivery-type">
									
									<label className="_label">Tipo de entrega * 
									 <button type="button" className="btn btn-transparent _disabled">
										<FontAwesomeIcon icon={faQuestionCircle}></FontAwesomeIcon>
									</button>
								  </label>
									
								<div className="_wrap-check-radio">
									
									<label className="_check-radio">
										<input type="checkbox" className="form-check-input" />
										<span className="overlay"></span>
										<div className="_wrap-alt-icon">
											<div className="_alt-icon">
												<FontAwesomeIcon icon={faTruck}></FontAwesomeIcon>
											</div>
											Operador logístico	
										</div>
									  
										<span className="check">
										   	<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
										</span>
								  
								  </label>
									
									<label className="_check-radio">
										<input type="checkbox" className="form-check-input" />
										<span className="overlay"></span>
										<div className="_wrap-alt-icon">
											<div className="_alt-icon">
												<FontAwesomeIcon icon={faMobile}></FontAwesomeIcon>
											</div>
											Aplicativo de entrega	
										</div>
									  
										<span className="check">
										   	<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
										</span>
								  
								  </label>
									
									<label className="_check-radio">
										<input type="checkbox" className="form-check-input" />
										<span className="overlay"></span>
										<div className="_wrap-alt-icon">
											<div className="_alt-icon">
												<FontAwesomeIcon icon={faHandHoldingBox}></FontAwesomeIcon>
											</div>
											Entrega em mãos	
										</div>
									  
										<span className="check">
										   	<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
										</span>
								  
								  </label>
									
								</div>	
								</div>
								
								<div className="form-group _photos">
									<div className="_label">
										Fotos *
										<small className="form-text text-muted">Upload máximo de 3 fotos.</small>
									</div>
									
									<div className="_wrap-photo-item row">
										
										<div className="_photo-item col col-3">

											<input type="file" accept="image/x-png, image/jpeg" name="photos" id="photos" multiple="multiple" className="form-control-file" />
											
											<div className="_image-selected">
												<img src={ProductIMG} alt="" />
												
												<button type="button" className="btn btn-transparent _delete-image">
													<FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon>
												</button>
											</div>
											
											<label htmlFor="photos">

												<FontAwesomeIcon icon={faCamera}></FontAwesomeIcon>

												<div className="_title-label">
													Adicionar fotos
												</div>

												<div className="_accept-formats-label">
													JPG, GIF E PNG Somente
												</div>
											</label>

										</div>
										
										<div className="_photo-item col col-3">

											<input type="file" accept="image/x-png, image/jpeg" name="photos" id="photos" multiple="multiple" className="form-control-file" />
											
											<div className="_image-selected no-photo">
												<img src={ProductIMG} alt="" />
												
												<button type="button" className="btn btn-transparent _delete-image">
													<FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon>
												</button>
											</div>
											
											<label htmlFor="photos">

												<FontAwesomeIcon icon={faCamera}></FontAwesomeIcon>

												<div className="_title-label">
													Adicionar fotos
												</div>

												<div className="_accept-formats-label">
													JPG, GIF E PNG Somente
												</div>
											</label>

										</div>
										
										<div className="_photo-item col col-3">

											<input type="file" accept="image/x-png, image/jpeg" name="photos" id="photos" multiple="multiple" className="form-control-file" />
											
											<div className="_image-selected no-photo">
												<img src={ProductIMG} alt="" />
												
												<button type="button" className="btn btn-transparent _delete-image">
													<FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon>
												</button>
											</div>
											
											<label htmlFor="photos">

												<FontAwesomeIcon icon={faCamera}></FontAwesomeIcon>

												<div className="_title-label">
													Adicionar fotos
												</div>

												<div className="_accept-formats-label">
													JPG, GIF E PNG Somente
												</div>
											</label>

										</div>
										
										<div className="_photo-item col col-3">

											<input type="file" accept="image/x-png, image/jpeg" name="photos" id="photos" multiple="multiple" className="form-control-file" />
											
											<div className="_image-selected no-photo">
												<img src={ProductIMG} alt="" />
												
												<button type="button" className="btn btn-transparent _delete-image">
													<FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon>
												</button>
											</div>
											
											<label htmlFor="photos">

												<FontAwesomeIcon icon={faCamera}></FontAwesomeIcon>

												<div className="_title-label">
													Adicionar fotos
												</div>

												<div className="_accept-formats-label">
													JPG, GIF E PNG Somente
												</div>
											</label>

										</div>
										
										<div className="_photo-item col col-3">

											<input type="file" accept="image/x-png, image/jpeg" name="photos" id="photos" multiple="multiple" className="form-control-file" />
											
											<div className="_image-selected no-photo">
												<img src={ProductIMG} alt="" />
												
												<button type="button" className="btn btn-transparent _delete-image">
													<FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon>
												</button>
											</div>
											
											<label htmlFor="photos">

												<FontAwesomeIcon icon={faCamera}></FontAwesomeIcon>

												<div className="_title-label">
													Adicionar fotos
												</div>

												<div className="_accept-formats-label">
													JPG, GIF E PNG Somente
												</div>
											</label>

										</div>
										
									</div>
									
							  </div>
								
								<div className="form-group">
								 <label htmlFor="price" className="_label">Localização</label>
								 <input type="email" id="price" className="form-control" placeholder="CEP" />
									
								<div className="_zipcode-address">
									<div className="_zipcode-icon">
										<FontAwesomeIcon icon={faMapMarkerAlt}></FontAwesomeIcon>
									</div>
									<div className="_zipcode-text">
										Bairro Água Verde, Curitiba - Paraná
									</div>
								</div>	
							  </div>
								
							<div className="_scamb-rates">
								<Link to="/" className="_scamb-rates-link">
									Detalhes taxa de administração scamb
									<button type="button" className="btn btn-transparent _disabled">
										<FontAwesomeIcon icon={faQuestionCircle}></FontAwesomeIcon>
									</button>
								</Link>
							</div>
								
							<div className="_ad-terms">
								Ao publicar voce concorda e aceita os nossos <Link to="/">termos</Link> de uso e <Link to="/">privacidade</Link>
							</div>
								
								
								<div className="form-group-button">
									<button type="submit" className="btn btn-primary">Criar anúncio</button>
								</div>	
								
							</form>
							
						</div>
						
					</div>
					
				</div>
				
			</div>

		);
		
	};
	
};

export default CreateAd;