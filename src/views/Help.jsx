import React, { Component } from 'react';
import Breadcrumb from '../components/Breadcrumbs';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import {
        faShoppingBag,
        faShoppingBasket,
        faMegaphone,
        faSearch } from '@fortawesome/pro-light-svg-icons';

class Help extends Component {
 
    render() {
        
        return (
            
            <section id="view_help">
                
                <Breadcrumb></Breadcrumb>
                
                <div className="_med-container">
                    
                    <section className="_wrap-help-search">
                        <h1 className="_title">
                            Com qual tema você quer ajuda?
                        </h1>
                        
                        <div className="_help-search">
                            {/* Search form */}
                            <div className="search-menu">
                                
                                <form className="form">
                                    <div className="form-group search">
                                        <input type="text" placeholder="Digite o que esta buscando" />
                                        <button type="submit">
                                            <FontAwesomeIcon icon={faSearch} className="fa-question-circle"/>
                                        </button>
                                    </div>
                                </form>
                                
                            </div>{/* END ./ Search form */}
                        </div>
                        
                    </section>
                    
                    <section className="_help-boxes">
                        
                        <div className="_help-box">
                            <div className="_box-title">
                                Sobre o Scamb
                                <button type="button" className="btn btn-transparent _disabled">
                                    <FontAwesomeIcon icon={faMegaphone}></FontAwesomeIcon>
                                </button>
                            </div>

                            <div className="_box-content">
                                <p className="_text">
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Aenean turpis tellus,
                                    elementum hendrerit feugiat.
                                </p>

                                <a href="#!">Saiba mais</a>
                            </div>

                        </div>
                        
                        <div className="_help-box">
                            <div className="_box-title">
                                Compras
                                <button type="button" className="btn btn-transparent _disabled">
                                    <FontAwesomeIcon icon={faShoppingBasket}></FontAwesomeIcon>
                                </button>
                            </div>

                            <div className="_box-content">
                                <p className="_text">
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Aenean turpis tellus,
                                    elementum hendrerit feugiat.
                                </p>

                                <a href="#!">Saiba mais</a>
                            </div>

                        </div>
                        
                        <div className="_help-box">
                            <div className="_box-title">
                                Vendas
                                <button type="button" className="btn btn-transparent _disabled">
                                    <FontAwesomeIcon icon={faShoppingBag}></FontAwesomeIcon>
                                </button>
                            </div>

                            <div className="_box-content">
                                <p className="_text">
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Aenean turpis tellus,
                                    elementum hendrerit feugiat.
                                </p>

                                <a href="#!">Saiba mais</a>
                            </div>

                        </div>
                        
                    </section>
                    
                    <section className="_help-content">
                        
                        <h1 className="_title">Escolha um topico</h1>
                        
                        <div className="_wrap-topics">
                            
                            <div className="_topics">
                                
                                <ul className="_topics-list">
                                    <li>
                                        <a href="#!" className="_item">
                                            <div className="_item-name">Compras</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#!" className="_item">
                                            <div className="_item-name">Devoluções</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#!" className="_item">
                                            <div className="_item-name">Opniões</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#!" className="_item">
                                            <div className="_item-name">Direito de arrependimento de compra</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#!" className="_item">
                                            <div className="_item-name">Fale com o suporte</div>
                                        </a>
                                    </li>
                                </ul>
                                
                            </div>
                        </div>
                        
                    </section>
                    
                </div>
            </section>
        
        );
        
    };
    
};

export default Help;