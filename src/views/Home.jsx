import React, { Component }                 from 'react';
import { Link }                             from 'react-router-dom';
import TopCategories                        from '../components/TopCategoriesHome';

import Product from '../components/GridProduct';
import OfficialStore from '../components/OfficialStore';
import SmallProduct from '../components/OfficialStoreProducts';

import Slider from "react-slick";

import { FontAwesomeIcon }                  from '@fortawesome/react-fontawesome';

// Icons
import { faTimes } from '@fortawesome/pro-light-svg-icons';

//Imagens
import Banner                               from '../assets/images/banner_1.png';
import BannerMobile                         from '../assets/images/mobile-banner.png';
import BannerNewAcc                         from '../assets/images/banner-new-account.png';

// Oficial Store #1
import BannerOficialStore_1                   from '../assets/images/banner-loja-oficial.png';
import LogoOficialStore_1                     from '../assets/images/logo-oficial-store-1.png';
import ThumbOficialStore_1_1                    from '../assets/images/store-1-tb-1.png';
import ThumbOficialStore_1_2                    from '../assets/images/store-1-tb-2.png';
import ThumbOficialStore_1_3                    from '../assets/images/store-1-tb-3.png';

// Oficial Store #2
import BannerOficialStore_2                   from '../assets/images/banner-loja-oficial-2.png';
import LogoOficialStore_2                     from '../assets/images/logo-oficial-store-2.png';
import ThumbOficialStore_2_1                    from '../assets/images/store-2-tb-1.png';
import ThumbOficialStore_2_2                    from '../assets/images/store-2-tb-2.png';
import ThumbOficialStore_2_3                    from '../assets/images/store-2-tb-3.png';

// Oficial Store #3
import BannerOficialStore_3                   from '../assets/images/banner-loja-oficial-3.png';
import LogoOficialStore_3                     from '../assets/images/logo-oficial-store-3.png';
import ThumbOficialStore_3_1                    from '../assets/images/store-3-tb-1.png';
import ThumbOficialStore_3_2                    from '../assets/images/store-3-tb-2.png';
import ThumbOficialStore_3_3                    from '../assets/images/store-3-tb-3.png';

// Oficial Store #3
import BannerOficialStore_4                   from '../assets/images/banner-loja-oficial-4.png';
import LogoOficialStore_4                     from '../assets/images/logo-oficial-store-4.png';
import ThumbOficialStore_4_1                    from '../assets/images/store-4-tb-1.png';
import ThumbOficialStore_4_2                    from '../assets/images/store-4-tb-2.png';

// Product Images default
import ProductImage                         from '../assets/images/product.png';

// Product Images
import ProductImage_0                         from '../assets/images/product_0.png';
import ProductImage_1                         from '../assets/images/product_1.png';
import ProductImage_2                         from '../assets/images/product_2.png';
import ProductImage_3                         from '../assets/images/product_3.png';
import ProductImage_4                         from '../assets/images/product_4.png';

const paramsSliderHome = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
	arrows: true
}

const paramsOfficialStores = {
    dots: false,
    infinite: false,
    speed: 500,
	arrows: true,
	variableWidth: true,
	responsive: [{
		breakpoint: 480,
		settings: {
			slidesToShow: 1,
    		slidesToScroll: 1,
		}
	}]
}

const paramsRecentlyAdded = {
	dots: false,
    infinite: false,
    speed: 500,
	arrows: true,
	variableWidth: true,
	responsive: [{
		breakpoint: 480,
		settings: {
			slidesToShow: 1,
    		slidesToScroll: 1,
		}
	}]
	
}

class Home extends Component {
	
    render () {
        
        return (
            
            <div id="view_home">
			    
                <div className="new-account-banner">
                    <div className="_wrap-new-account-banner">
                        
                        <button type="button" className="btn btn-transparent _close-modal">
                            <FontAwesomeIcon icon={faTimes} />
                        </button>
                        
                        <div className="_wrap-new-account-banner--inside">

                            <img src={BannerNewAcc} alt="" />
                            <Link to="/criar-novo" className="btn btn-neutral _fw600"> Criar minha conta</Link>

                        </div>
                    </div>
                </div>
                
                <div id="hero-slider">
                    <Slider {...paramsSliderHome} className="slider-container">
                        <Link to="./prosseguir" className="slider-item">
                            <picture>
                                <source media="(max-width: 450px)" srcSet={BannerMobile} />
                                <img src={Banner} alt="Banner Torne-se um scamber" />
                            </picture>
                        </Link>
                        <Link to="./prosseguir" className="slider-item">
                            <picture>
                                <source media="(max-width: 450px)" srcSet={BannerMobile} />
                                <img src={Banner} alt="Banner Torne-se um scamber" />
                            </picture>
                        </Link>
                    </Slider >
                </div>    
                    
                <div className="_med-container">  
                    
                    <div className="_wrap-top-categories">  
                    
                        <h1 className="headline _color _gray-bolder _fw700">TOP Categorias</h1>
                        
                        <TopCategories></TopCategories>
                    
                    </div>

                    <div className="_wrap-recently-added">
                    
                    <h1 className="headline _color _gray-bolder _fw700">Adicionados recentemente <Link to="/minha-loja">ver mais</Link> </h1>

                        {/* Recently Added */}
                        <Slider {...paramsRecentlyAdded} className="_recently-added">

                            <Product link="/produto" image={ProductImage_0} title="short listrado jeans" discount="100" price="68" ></Product>

                            <Product link="/produto" image={ProductImage_1} title="vaso cactus" discount="100" price="60" ></Product>
                            
                            <Product link="/produto" image={ProductImage_2} title="fone wireless" discount="100" price="700" ></Product>
                            
                            <Product link="/produto" image={ProductImage_3} title="cadeira charles eames" discount="100" price="480" ></Product>
                            
                            <Product link="/produto" image={ProductImage_4} title="quadros para decoraçào" discount="100" price="220" ></Product>
                            
                            <Product link="/produto" image={ProductImage} title="Cadeira Amarela Charles Eames Eiffel" discount="100" price="120" ></Product>
                            
                        </Slider>{/* END ./ Recently Added */}

                    </div>
                    
                    <div className="_wrap-official-stores">
                    
                        <h1 className="headline _color _gray-bolder _fw700">Lojas oficiais <Link to="/lojas-oficiais">ver mais</Link> </h1>
                        
						<Slider {...paramsOfficialStores} className="inline-slider _official-stores">

                            <OfficialStore link="/lojas-oficiais" banner={BannerOficialStore_1} logo={LogoOficialStore_1} title="Vic & Lolo">
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_1_1} price="109"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_1_2} price="104"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_1_3} price="129"></SmallProduct>
                            </OfficialStore>

                            <OfficialStore link="/lojas-oficiais" banner={BannerOficialStore_2} logo={LogoOficialStore_2} title="de Casa">
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_2_1} price="149"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_2_2} price="104"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_2_3} price="229"></SmallProduct>
                            </OfficialStore>
                            
                            <OfficialStore link="/lojas-oficiais" banner={BannerOficialStore_3} logo={LogoOficialStore_3} title="Meu Desapego">
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_3_1} price="339"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_3_2} price="49"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_3_3} price="98"></SmallProduct>
                            </OfficialStore>
                            
                            <OfficialStore link="/lojas-oficiais" banner={BannerOficialStore_4} logo={LogoOficialStore_4} title="Bazar">
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_4_1} price="139"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_4_2} price="49"></SmallProduct>
                                <SmallProduct link="/xs1215" image={ThumbOficialStore_3_3} price="92"></SmallProduct>
                            </OfficialStore>

                        </Slider>
							
                    </div>
                    
                </div>

            </div>
            
        
        )
        
    }
    
}

export default Home;