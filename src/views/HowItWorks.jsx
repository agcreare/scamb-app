import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Breadcrumb from '../components/Breadcrumbs';

class HowItWorks extends Component {
    
    render() {
        
        return (
            
            <section id="view_common-page">
                
                <Breadcrumb></Breadcrumb>
                
                <div className="_med-container">
                    <div className="_terms-conditions-content">

                        <h1 className="headline">Vender no scamb é facil!</h1>
                        
                        <div className="_wrap-topic">
                            <p className="_topic-content">
                                Veja como funciona em um passo a passo<br/> bem explicado abaixo.
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Antes de começar a vender
                            </h1>
                            <p className="_topic-content">
                                Antes de começar a vender você precisa criar uma conta e fazer algumas configurações em sua loja.<br/>
                                Mas fica calmo que é tudo super rápido e fácil!
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Criei e configurei minha loja. e agora?
                            </h1>
                            <p className="_topic-content">
                                Agora e só clicar no botão criar novo
                                anúncio e seguir a passo a passo.<br/>  Em
                                menos de 5 minutos seu produto ja está
                                pronto para vender! Rápido não é mesmo?
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Qual a moeda do Scamb?
                            </h1>
                            <p className="_topic-content">
                                A moeda do Scamb são os pontos - 1 ponto
                                é igual a 1 Real.<br/>  Um exemplo prático seria,
                                se você tem um produto que vale 100 Reais,
                                ele valerá 100 pontos na plataforma.
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Qual o próximo passo?
                            </h1>
                            <p className="_topic-content">
                                <Link to="/criar-conta" className="btn btn-primary">Criar uma conta</Link>
                            </p>
                        </div>
                        
                    </div>
                </div>
                
            </section>
            
        );
        
    };
    
};

export default HowItWorks;