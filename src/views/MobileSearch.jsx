import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import { faArrowLeft, faTimes, faSearch, faClone } from '@fortawesome/pro-light-svg-icons';

//Images
import ProductIMG2 from '../assets/images/product-single-2.png';

class MobileSearch extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
          slider: null,
          sliderThumbs: null
        };
      }
    
      componentDidMount() {
        this.setState({
          slider: this.featuredSlider,
          sliderThumbs: this.sliderThumbnail
        });
      }

    componentDidMount() {

        var current                 = null;
        var categories              = $('._wrap-categories');
        var suggestions_box         = $('._suggestions-wrapper');
        var suggestions_list_item   = $('._suggestions-list-item');
        var field_search            = $('._field-search');
        var clear_field             = $('#_clear-field');

        $(suggestions_list_item).on('click', function () {

            if(current != this) {
                current = this;
                var currentText = $(current).find('._suggestion').text()
                
                $(field_search).val(currentText);
                

            } else current = null;

        });

        $(clear_field).on('click', function () {
            $(field_search).val('');
            $(suggestions_box).addClass('_is-hidden');
            $(categories).removeClass('_is-hidden');
        });

        $(field_search).on('click', function () {
            $(suggestions_box).removeClass('_is-hidden');
            $(categories).addClass('_is-hidden');
        })
    }

    render() {

        return (

            <section id="view_search">
                <div className="_wrap-search">
                    
                    <form className="_form">
                        <div className="form-group">
                                        
                        <div className="_wrap-input-group">
                            <div className="input-group">

                                <div className="input-group-prepend">
                                    <Link to="/" className="input-group-text" id="mob-search">
                                        <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                                    </Link>
                                </div>
                                <input type="text" className="form-control _field-search" placeholder="Digite o que esta buscando" aria-describedby="mob-search" />

                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="_clear-field">
                                    <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                                    </span>
                                </div>
                            </div>
                        </div>
                            
                        </div>

                        <div className="_suggestions-wrapper _is-hidden">
                            <ul className="_suggestions-list">
                                <li>
                                   <Link className="_suggestions-list-item">
                                        <div className="_wrap-suggestion">
                                        <span className="_icon-left">
                                            <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>
                                        </span>

                                        <div className="_suggestion">
                                            Carrinho com <span className="bold">bebe comforto</span> 
                                        </div>
                                        </div>

                                       <span className="_icon-right">
                                        <FontAwesomeIcon icon={faClone}></FontAwesomeIcon>
                                       </span>

                                   </Link> 
                                </li>
                                <li>
                                   <Link className="_suggestions-list-item">
                                        <div className="_wrap-suggestion">
                                        <span className="_icon-left">
                                            <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>
                                        </span>

                                        <div className="_suggestion">
                                            Carrinho <span className="bold">guarda-chuva</span> 
                                        </div>
                                        </div>

                                       <span className="_icon-right">
                                        <FontAwesomeIcon icon={faClone}></FontAwesomeIcon>
                                       </span>

                                   </Link> 
                                </li>
                                <li>
                                   <Link className="_suggestions-list-item">
                                        <div className="_wrap-suggestion">
                                        <span className="_icon-left">
                                            <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>
                                        </span>

                                        <div className="_suggestion">
                                            Carrinho <span className="bold">gêmeos</span> 
                                        </div>
                                        </div>

                                       <span className="_icon-right">
                                        <FontAwesomeIcon icon={faClone}></FontAwesomeIcon>
                                       </span>

                                   </Link> 
                                </li>
                                <li>
                                   <Link className="_suggestions-list-item">
                                        <div className="_wrap-suggestion">
                                        <span className="_icon-left">
                                            <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>
                                        </span>

                                        <div className="_suggestion">
                                            <span className="bold">Protetor</span>  Carrinho
                                        </div>
                                        </div>

                                       <span className="_icon-right">
                                        <FontAwesomeIcon icon={faClone}></FontAwesomeIcon>
                                       </span>

                                   </Link> 
                                </li>
                            </ul>
                        </div>
                    </form>

                <div className="_wrap-categories">
                <Row>
                        
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                perto de mim
                            </div>
                        </Link>
                    </Col>
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                lojas que sigo
                            </div>
                        </Link>
                    </Col>
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                lojas mais seguidas
                            </div>
                        </Link>
                    </Col>
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                beleza e saúde
                            </div>
                        </Link>
                    </Col>
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                roupas e etc
                            </div>
                        </Link>
                    </Col>
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                kids
                            </div>
                        </Link>
                    </Col>
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                casa e decoração
                            </div>
                        </Link>
                    </Col>
                    <Col xs="6" md="4" lg="3">
                        <Link to="/resultados" className="_category-search-item">
                            <div className="_item-image">
                                <img src={ProductIMG2} alt="" />
                            </div>
                            <div className="_item-footer">
                                outros
                            </div>
                        </Link>
                    </Col>

                </Row>
                </div>


                </div>
            </section>

        );

    };

};

export default MobileSearch;