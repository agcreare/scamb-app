import React, { Component, setState } from "react";
import { Link } from "react-router-dom";
import Breadcrumb from "../components/Breadcrumbs";
import MessengerInbox from "../components/MessengerInbox";
import ModalBase from "../components/modals/ModalBase";
import ModalTaxs from "./modals/ModalTaxs";
import ModalShippingOptions from "./modals/ModalShippingOptions";
import ProductItem from '../components/GridProduct';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Slider from "react-slick";

// Icons
import {
  faQuestionCircle,
  faInfoCircle,
  faTruck,
  faMobile,
  faHandHoldingBox,
  faHeart,
  faChevronRight,
  faStore,
  faCheck,
  faLongArrowLeft,
  faShare,
  faInbox,
} from "@fortawesome/pro-light-svg-icons";

// Icons
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
} from "@fortawesome/free-brands-svg-icons";

//Images
import LogoStore from "../assets/images/logo-store.png";
import ProductIMG from "../assets/images/product-single-img-1.png";
import ProductIMG2 from "../assets/images/product-single-img-2.png";

// Product Images
import ProductImage                         from '../assets/images/product.png';
import ProductImage_1 from "../assets/images/product_1.png";
import ProductImage_2 from "../assets/images/product_2.png";
import ProductImage_3 from "../assets/images/product_3.png";
import ProductImage_4 from "../assets/images/product_4.png";

class Product extends Component {
  constructor(props) {
    super(props);
    this.childProposal = React.createRef();
    this.childTaxs = React.createRef();
    this.childShippingOptions = React.createRef();
    this.childMessenger = React.createRef();
    this.state = {
      slider: null,
      sliderThumbs: null,
      showSharedButtons: false,
      messenger: false,
    };
  }

  openModalProposal = () => {
    this.childProposal.current.handleShow();
  };

  closeModalProposal = () => {
    this.childProposal.current.handleClose();
  };

  openModalTaxs = () => {
    this.childTaxs.current.handleShow();
  };

  openModalShippingOptions = () => {
    this.childShippingOptions.current.handleShow();
  };

  handleToggleShared = () => {
    this.setState({
      showSharedButtons: !this.state.showSharedButtons,
    });
  };

  handleMessengerClick = () => {
    this.childMessenger.current.handleShowMessenger();
  };

  componentDidMount() {
    this.setState({
      slider: this.featuredSlider,
      sliderThumbs: this.sliderThumbnail,
    });
  }

  render() {
    const paramsRecentlyAdded = {
      dots: false,
      infinite: false,
      speed: 500,
      arrows: true,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };

    return (
      <div id="view_product">
        <Breadcrumb></Breadcrumb>

        <section className="_wrap-messenger-inbox">
          <MessengerInbox ref={this.childMessenger}></MessengerInbox>
        </section>

        <div className="layout-main">
          <div className="layout-col layout-col-left">
            <div className="_wrap-product-gallery">
              <div className="go-back">
                <Link to="/" className="btn btn-transparent">
                  <FontAwesomeIcon icon={faLongArrowLeft}></FontAwesomeIcon>
                </Link>
              </div>

              <Slider
                arrows={false}
                adaptiveHeight={true}
                fade={true}
                dots={true}
                asNavFor={this.state.sliderThumbs}
                ref={(slider) => (this.featuredSlider = slider)}
                className="product-gallery"
              >
                <div className="product-gallery-item">
                  <img src={ProductIMG} alt="" />
                </div>
                <div className="product-gallery-item">
                  <img src={ProductIMG2} alt="" />
                </div>
              </Slider>

              <Slider
                asNavFor={this.state.slider}
                ref={(slider) => (this.sliderThumbnail = slider)}
                slidesToShow={2}
                swipeToSlide={true}
                focusOnSelect={true}
                className="slider-thumbnails"
              >
                <div className="slider-thumbnail-item">
                  <img src={ProductIMG} alt="" />
                </div>
                <div className="slider-thumbnail-item">
                  <img src={ProductIMG2} alt="" />
                </div>
              </Slider>
            </div>
          </div>

          <div className="layout-col layout-col-right">
            <div className="_buttons-actions">
              <div className="share">
                <button
                  type="button"
                  className="btn btn-transparent _disabled"
                  onClick={this.handleToggleShared}
                >
                  <FontAwesomeIcon icon={faShare}></FontAwesomeIcon>
                </button>

                {this.state.showSharedButtons ? (
                  <ul className="_social-share-list">
                    <li className="_share-item">
                      <Link to="/">
                        <FontAwesomeIcon icon={faFacebookF}></FontAwesomeIcon>
                      </Link>
                    </li>
                    <li className="_share-item">
                      <Link to="/">
                        <FontAwesomeIcon icon={faInstagram}></FontAwesomeIcon>
                      </Link>
                    </li>
                    <li className="_share-item">
                      <Link to="/">
                        <FontAwesomeIcon icon={faLinkedinIn}></FontAwesomeIcon>
                      </Link>
                    </li>
                  </ul>
                ) : null}
              </div>

              <div className="add-to-favorites">
                <button type="button" className="btn btn-transparent _disabled">
                  <FontAwesomeIcon icon={faHeart}></FontAwesomeIcon>
                </button>
              </div>
            </div>

            <section className="_short-description">
              <Link
                to="/"
                className="small-link-store _color _gray _12px _fw700"
              >
                Lojinha Emilia
              </Link>

              <div className="item-title">
                <h1 className="item-title--primary _color _black _fw400">
                  short listrado jeans
                </h1>
              </div>

              <div className="item-price">
                <div className="_product-price _2x">
                  <div className="ui-item__discount-price">
                    <span className="price-tag-fraction">89</span>
                    <span className="price-tag-symbol">pts</span>
                  </div>
                  <div className="ui-item__price">
                    <span className="price-tag-fraction">68</span>
                    <span className="price-tag-symbol">pts</span>
                  </div>
                </div>
              </div>

              <div className="tax-scamb">
                <div className="tax-scamb--title _color _gray _11px">
                  Taxa Scamb:
                  <span className="_12px _fw700">R$6.90</span>
                  <button
                    className="btn btn-transparent"
                    onClick={this.openModalTaxs}
                  >
                    <FontAwesomeIcon icon={faQuestionCircle}></FontAwesomeIcon>
                  </button>
                </div>
              </div>

              <div className="insufficient-points">
                <div className="insufficient-points--title _color _gray _12px">
                  Não tem pontos suficientes?
                  <Link
                    to="/comprar-pontos"
                    className="redirect-to-buy--points _color _blue _fw700"
                  >
                    Clique aqui
                  </Link>
                </div>
              </div>

              <div className="shipping-options">
                <div className="shipping-options--title _color _gray _12px">
                  Opções de frete disponíveis
                  <button
                    className="btn btn-transparent"
                    onClick={this.openModalShippingOptions}
                  >
                    <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
                  </button>
                </div>

                <div className="shipping-options-items">
                  <div className="shipping-options-item _active">
                    <div className="item-icon">
                      <FontAwesomeIcon icon={faTruck}></FontAwesomeIcon>
                    </div>
                    <div className="item-info">Operador logístico</div>
                  </div>

                  <div className="shipping-options-item _active">
                    <div className="item-icon">
                      <FontAwesomeIcon icon={faMobile}></FontAwesomeIcon>
                    </div>
                    <div className="item-info">Aplicativo de entrega</div>
                  </div>

                  <div className="shipping-options-item">
                    <div className="item-icon">
                      <FontAwesomeIcon
                        icon={faHandHoldingBox}
                      ></FontAwesomeIcon>
                    </div>
                    <div className="item-info">Entrega em mãos</div>
                  </div>
                </div>
              </div>

              <div className="_wrap-actions">
                <Link to="/comprar" className="btn btn-red btn-buy _fw700">
                  Comprar
                </Link>
                <button
                  type="button"
                  className="btn btn-red btn-border make-offer _fw700"
                  onClick={this.openModalProposal}
                >
                  Fazer oferta
                </button>
                <div className="_wrap-chat-button">
                  <button
                    type="button"
                    className="btn btn-neutral btn-border _fw700 _btn-chat"
                    onClick={this.handleMessengerClick}
                  >
                    <span>Iniciar chat</span>
                    <FontAwesomeIcon icon={faInbox}></FontAwesomeIcon>
                  </button>
                </div>
              </div>
            </section>
          </div>
        </div>

        <div className="layout-main-informations _med-container">
          <div className="layout-col layout-col-left">
            <section className="_long-description">
              <h1 className="_long-descriptionr-title _title _color _black _fw700">
                Descrição
              </h1>

              <p className="_long-description-text _color _black _fw600">
                short azul e branco confeccionado em jeans, o modelo possui
                fecho de botões vista dupla, dois bolsos frontais e dois
                posteriores, barra desfiada e estampa de listras.
                <br />
                <br />
                composição: algodão
                <br />
                <br />
                medidas: cintura: 80.0cm comprimento: 29.0cm quadril: 98.0cm
              </p>
            </section>

            <section className="_characteristics">
              <h1 className="_characteristics-title _title _color _black _fw700">
                Características
              </h1>

              <ul className="_characteristics-list">
                <li>
                  <div className="_key">marca</div>
                  <div className="_value">basico.com</div>
                </li>
                <li>
                  <div className="_key">Condição</div>
                  <div className="_value">Usado</div>
                </li>
                <li>
                  <div className="_key">Categoria</div>
                  <div className="_value">roupas e etc</div>
                </li>
              </ul>
            </section>

            <section className="_payment-methods">
              <h1 className="_payment-methods-title _title _color _black _fw700">
                Formas de pagamento
              </h1>

              <div className="_small-heading">
                Pontos Scamb
                <span className="_small-heading-bottom _color _gray">
                  <button className="btn btn-transparent">
                    <FontAwesomeIcon icon={faInfoCircle}></FontAwesomeIcon>
                  </button>
                  1 Ponto = R$ 1,00 Real
                </span>
              </div>
            </section>
          </div>

          <div className="layout-col layout-col-right">
            <section className="_advertiser">
              <div className="_advertiser-title _color _black _14px _fw700">
                Anunciante
              </div>

              <div className="_advertiser-card">
                <div className="_advertiser-card-logo">
                  <img src={LogoStore} alt="" />
                </div>
                <div className="_advertiser-card-title--location">
                  Loja Emilia
                  <span className="title-location">Curitiba - PR</span>
                </div>
                <div className="_advertiser-card-follow">
                  <button type="button" className="btn btn-follow">
                    Seguir
                  </button>
                </div>

                <div className="_advertiser-card-redirect">
                  <button type="button" className="btn">
                    <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
                  </button>
                </div>
              </div>
            </section>

            <section
              className="section-seller-info _off-status"
              style={{ display: "none" }}
            >
              <div className="seller-title _color _black _14px _fw700">
                Classicação
              </div>

              <div className="_wrap-seller-status">
                <div className="seller-status _color _gray _12px _fw700">
                  Este vendendor ainda não possui vendas suficientes para gerar
                  um historico de reputação.
                </div>
              </div>
            </section>
            <section className="section-seller-info">
              <div className="seller-title _color _black _14px _fw700">
                Classicação
              </div>

              <div className="_wrap-seller-status">
                <div className="seller-status _color _gray _12px _fw700">
                  É um dos melhores do site!
                </div>

                <ul className="reputation-thermometer">
                  <li className="reputation-thermometer-item reputation-thermometer-1">
                    <span>Vermelho</span>
                  </li>
                  <li className="reputation-thermometer-item reputation-level reputation-thermometer-2">
                    <span>Laranja</span>
                  </li>
                  <li className="reputation-thermometer-item reputation-thermometer-3">
                    <span>Amarelo</span>
                  </li>
                  <li className="reputation-thermometer-item reputation-thermometer-4">
                    <span>Verde claro</span>
                  </li>
                  <li className="reputation-thermometer-item reputation-thermometer-5">
                    <span>Verde</span>
                  </li>
                </ul>
              </div>

              <div className="reputation-info-items">
                <div className="reputation-info-item">
                  <div className="reputation-info-item--wrap">
                    <div className="item-icon">
                      <FontAwesomeIcon icon={faStore}></FontAwesomeIcon>
                      <span className="check-mark">
                        <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                      </span>
                    </div>
                    <div className="item-text _color _gray _10px text-center _fw700">
                      3561 Produtos Vendidos
                    </div>
                  </div>
                </div>

                <div className="reputation-info-item">
                  <div className="reputation-info-item--wrap">
                    <div className="item-icon">
                      <FontAwesomeIcon icon={faStore}></FontAwesomeIcon>
                      <span className="check-mark">
                        <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                      </span>
                    </div>
                    <div className="item-text _color _gray _10px text-center _fw700">
                      3561 Produtos Vendidos
                    </div>
                  </div>
                </div>

                <div className="reputation-info-item">
                  <div className="reputation-info-item--wrap">
                    <div className="item-icon">
                      <FontAwesomeIcon icon={faStore}></FontAwesomeIcon>
                      <span className="check-mark">
                        <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                      </span>
                    </div>
                    <div className="item-text _color _gray _10px text-center _fw700">
                      3561 Produtos Vendidos
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>

        <div className="_wrap-more-products">
          <div className="_med-container">
            <section className="more-products">
              <h1 className="headline _color _gray-bolder _fw700">
                Mais produtos do vendedor <Link to="/minha-loja">ver mais</Link>
              </h1>
              {/* Recently Added */}
              <Slider {...paramsRecentlyAdded} className="_recently-added">
                <ProductItem
                  link="/produto"
                  image={ProductImage_1}
                  title="vaso cactus"
                  discount="100"
                  price="60"
                ></ProductItem>

                <ProductItem
                  link="/produto"
                  image={ProductImage_2}
                  title="fone wireless"
                  discount="100"
                  price="700"
                ></ProductItem>

                <ProductItem
                  link="/produto"
                  image={ProductImage_3}
                  title="cadeira charles eames"
                  discount="100"
                  price="480"
                ></ProductItem>

                <ProductItem
                  link="/produto"
                  image={ProductImage_4}
                  title="quadros para decoraçào"
                  discount="100"
                  price="220"
                ></ProductItem>

                <ProductItem
                  link="/produto"
                  image={ProductImage}
                  title="Cadeira Amarela Charles Eames Eiffel"
                  discount="100"
                  price="120"
                ></ProductItem>
              </Slider>
              {/* END ./ Recently Added */}
            </section>
          </div>
        </div>

        {/* Modal Proposta Produto */}
        <ModalBase
          ref={this.childProposal}
          modalTitle="Fazer oferta"
          modalSubTitle="seja justo e faça a sua melhor oferta. se o vendedor topar, você leva!"
        >
          <form className="_form _form-proposal">
            <div className="_product">
              <div className="_product-image">
                <img src={ProductIMG} alt="" />
              </div>
              <div className="_product-info">
                <div className="_store-title">Loja Emilia</div>
                <div className="_product-title">short listrado jeans</div>
                <div className="_product-price _1x">
                  <div className="ui-item__price">
                    <span class="price-tag-fraction">68</span>
                    <span class="price-tag-symbol">pts</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="price" className="_label">
                Minha oferta *
              </label>
              <input
                type="text"
                id="price"
                className="form-control"
                placeholder="Digite o preço"
              />
            </div>
            <div className="form-group">
              <label htmlFor="message" className="_label">
                Mensagem *
              </label>
              <textarea
                id="message"
                className="form-control"
                placeholder="Escreva uma mensagem"
                rows="2"
              />
            </div>
            <div className="_form-footer">
              <Link to="/mensagens" className="btn btn-red">
                Confirmar
              </Link>
              <button
                type="button"
                className="btn btn-cancel"
                onClick={this.closeModalProposal}
              >
                Cancelar
              </button>
            </div>
          </form>
        </ModalBase>

        {/* Modal Taxas Scamb */}
        <ModalBase
          ref={this.childTaxs}
          modalTitle="Taxas de administração"
          modalSubTitle="
					Voce não paga nada para listar produtos no Scamb e ter uma lojinha ativa. Porém, a cada transação
					efetivada, o Scamb cobra uma taxa de administração para apoiar a manutenção da plataforma. No
					momento da confirmação da transação, o valor é debitado automaticamente no cartão de crédito cadastrado.
				"
        >
          <ModalTaxs></ModalTaxs>
        </ModalBase>

        <ModalBase
          ref={this.childShippingOptions}
          modalTitle="Opções de entrega"
          modalSubTitle="Qual a  maneira mais prática para vocês?"
        >
          <ModalShippingOptions></ModalShippingOptions>
        </ModalBase>
      </div>
    );
  }
}

export default Product;
