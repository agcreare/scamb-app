import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Helmet} from 'react-helmet';

class Sobre extends Component {
    
    render () {
        
        return (
            
            <div>
                <Helmet>
                    
                    <meta charSet="utf-8" />
                    <title>Sobre - Scamb | Comprar coisas pagando com coisas!</title>
                    
                </Helmet>
                <h1>Sobre</h1>
                <Link to="/">Home</Link>
            </div>
        
        )
        
    }
    
}

export default Sobre;