import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import Header from '../components/account/Header';
import Product from '../components/GridProduct';
import Filter from '../utils/Filter';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import { faChevronDown, faCheck, faUser, faChevronRight, faHeart } from '@fortawesome/pro-light-svg-icons';

// Images
import ProductImage from '../assets/images/product.png';

class Store extends Component {
    
    componentDidMount() {
        
        var currentSelected = null;
        var current = null;
        var upSpeed = 300;
        
        $("._dropdown-select").click(function() {
            
          $(this).children("._form-select-content").slideDown(upSpeed);
        
          if (current != this) {
                $(current).children("._form-select-content").slideUp(upSpeed);
                current = this;
          } else {
              current = null
          };
            
        });
        
        $("._select-content").on('click', function () {
        
            let fieldValue = $(this).find("._current-item-name").text();
            
            if (currentSelected != this) {
                $(this).closest('._dropdown-select').find("._the-label > ._span-name").text(fieldValue);
                currentSelected = this;
            } else currentSelected = null;

        });
        
    }
    
    render() {
        
        return (
            
            <div id="view_store">
                <Header></Header>
                
                <section className="_store-content">
                    <div className="_wrap-filters">
                        
                        <h1 className="_filters-title"> Filtros </h1>

                        <form className="_form _sidebar">
                            
                                <Filter filterTitle="Categorias" filterType="_type-select">
                                        
                                    <div className="_filter-item _checkbox form-check">
                                        <label className="form-check-label">

                                            <input type="checkbox" className="form-check-input" />
                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>
                                            <span className="_name"> Roupas e etc </span>
                                        </label>
                                    </div>
                                    <div className="_filter-item _checkbox form-check">
                                        <label className="form-check-label">

                                            <input type="checkbox" className="form-check-input" />
                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>
                                            <span className="_name"> Kids </span>
                                        </label>
                                    </div>
                                    <div className="_filter-item _checkbox form-check">
                                        <label className="form-check-label">

                                            <input type="checkbox" className="form-check-input" />
                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>
                                            <span className="_name"> Beleza e saúde </span>
                                        </label>
                                    </div>
                                    <div className="_filter-item _checkbox form-check">
                                        <label className="form-check-label">

                                            <input type="checkbox" className="form-check-input" />
                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>
                                            <span className="_name"> Outros </span>
                                        </label>
                                    </div>
                                        
                                </Filter>

                                <Filter filterTitle="Condição" filterType="_type-select">
                                        
                                    <div className="_filter-item _checkbox form-check">
                                        <label className="form-check-label">

                                            <input type="checkbox" className="form-check-input" />
                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>
                                            <span className="_name"> Novo </span>
                                        </label>
                                    </div>
                                    <div className="_filter-item _checkbox form-check">
                                        <label className="form-check-label">

                                            <input type="checkbox" className="form-check-input" />
                                            <span className="check">
                                                <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
                                            </span>
                                            <span className="_name"> Usado </span>
                                        </label>
                                    </div>
                                        
                                </Filter>

                                <Filter filterTitle="Preço" filterType="_type-default">

                                    <div className="form-group">
                                    
                                        <div className="_wrap-min-max">

                                            <div className="_wrap-input-group">
                                                <label className="_label">de</label>
                                                <div className="input-group">

                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text" id="min-money">
                                                            R$
                                                        </span>
                                                    </div>
                                                    <input type="text" className="form-control" placeholder="" aria-describedby="min-money" />
                                                </div>
                                            </div>

                                            <div className="_wrap-input-group">
                                                <label className="_label">até</label>
                                                <div className="input-group">

                                                    <div className="input-group-prepend">
                                                        <span className="input-group-text" id="max-money">
                                                            R$
                                                        </span>
                                                    </div>
                                                    <input type="text" className="form-control" placeholder="" aria-describedby="max-money" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                </Filter>

                                <div className="_form-filter-footer">
                                    <button className="btn btn-primary">Aplicar</button>
                                    <button className="btn btn-cancel">Limpar</button>
                                </div>

                            </form>
                        
                    </div>
                    
                    <div className="_store-products">
                    <div className="_products">
                        
                        <Row>
                        
                            <Col xs="6" md="4" lg="3">
                                <Product link="/produto" image={ProductImage} title="vaso cactus" discount="100" price="90" ></Product>
                            </Col>
                        
                            <Col xs="6" md="4" lg="3">
                                <Product link="/produto" image={ProductImage} title="vaso cactus" discount="100" price="90" ></Product>
                            </Col>
                        
                            <Col xs="6" md="4" lg="3">
                                <Product link="/produto" image={ProductImage} title="vaso cactus" discount="100" price="90" ></Product>
                            </Col>
                        
                            <Col xs="6" md="4" lg="3">
                                <Product link="/produto" image={ProductImage} title="vaso cactus" discount="100" price="90" ></Product>
                            </Col>
                        
                            <Col xs="6" md="4" lg="3">
                                <Product link="/produto" image={ProductImage} title="vaso cactus" discount="100" price="90" ></Product>
                            </Col>
                        
                            <Col xs="6" md="4" lg="3">
                                <Product link="/produto" image={ProductImage} title="vaso cactus" discount="100" price="90" ></Product>
                            </Col>
                        
                            <Col xs="6" md="4" lg="3">
                                <Product link="/produto" image={ProductImage} title="vaso cactus" discount="100" price="90" ></Product>
                            </Col>
                            
                        </Row>
                    
                    </div>
                        <div className="_wrap-load-more">
                            <button type="submit" className="btn btn-red">Carregar mais</button>
                        </div>
                    </div>
                    
                </section>
            </div>
            
            
        );
        
    };
    
};

export default Store;