import React, { Component } from 'react';
import Breadcrumb from '../components/Breadcrumbs';

class TermsConditions extends Component {
    
    render() {
        
        return (
            
            <section id="view_common-page">
                
                <Breadcrumb></Breadcrumb>
                
                <div className="_med-container">
                    <div className="_terms-conditions-content">

                        <h1 className="headline">Termos e condições gerais de uso do site</h1>
                        
                        <div className="_tag-date">
                            Ultima Atualização: 02/05/2020
                        </div>
                        
                        <div className="_wrap-topic _first">
                            <p className="_topic-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean turpis tellus, elementum hendrerit feugiat in, eleifend sit amet orci.
                                Proin lacinia congue fermentum. Praesent ac purus eget mi consectetur imperdiet et sit amet diam. Suspendisse a neque libero. Mauris tincidunt
                                faucibus orci, quis cursus dolor efficitur vitae.
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Topico #1
                            </h1>
                            <p className="_topic-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean turpis tellus, elementum hendrerit feugiat in, eleifend sit amet orci.
                                Proin lacinia congue fermentum. Praesent ac purus eget mi consectetur imperdiet et sit amet diam. Suspendisse a neque libero. Mauris tincidunt
                                faucibus orci, quis cursus dolor efficitur vitae.
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Topico #2
                            </h1>
                            <p className="_topic-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean turpis tellus, elementum hendrerit feugiat in, eleifend sit amet orci.
                                Proin lacinia congue fermentum. Praesent ac purus eget mi consectetur imperdiet et sit amet diam. Suspendisse a neque libero. Mauris tincidunt
                                faucibus orci, quis cursus dolor efficitur vitae.
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Topico #3
                            </h1>
                            <p className="_topic-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean turpis tellus, elementum hendrerit feugiat in, eleifend sit amet orci.
                                Proin lacinia congue fermentum. Praesent ac purus eget mi consectetur imperdiet et sit amet diam. Suspendisse a neque libero. Mauris tincidunt
                                faucibus orci, quis cursus dolor efficitur vitae.
                            </p>
                        </div>
                        
                        <div className="_wrap-topic">
                            <h1 className="_topic-title">
                                Topico #4
                            </h1>
                            <p className="_topic-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean turpis tellus, elementum hendrerit feugiat in, eleifend sit amet orci.
                                Proin lacinia congue fermentum. Praesent ac purus eget mi consectetur imperdiet et sit amet diam. Suspendisse a neque libero. Mauris tincidunt
                                faucibus orci, quis cursus dolor efficitur vitae.
                            </p>
                        </div>
                        
                    </div>
                </div>
                
            </section>
            
        );
        
    };
    
};

export default TermsConditions;