import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import Chat from "../../components/account/messages/Chat";
import Message from "../../components/account/messages/Messages";
import News from "../../components/account/messages/News";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Icons
import { faTimes, faEnvelopeOpen } from "@fortawesome/pro-light-svg-icons";

// Images
import ProductProposalIMG from "../../assets/images/product-proposal.png";
import ProductProposalIMG1 from "../../assets/images/product-single-img-1.png";

const received_messages = [
  {
    _id: 1,
    status: "read",
    image: ProductProposalIMG1,
    message_date: "19/05/2020",
    username: "Marina",
    action: "proposal",
    product_title: "short listrado jeans",
  },
  {
    _id: 2,
    status: "read",
    image: ProductProposalIMG1,
    message_date: "18/05/2020",
    username: "Marina",
    action: "doubt",
    product_title: "short listrado jeans",
  },
  {
    _id: 3,
    status: "read",
    image: ProductProposalIMG,
    message_date: "17/05/2020",
    username: "paula",
    action: "purchase",
    product_title: "sapato",
  },
];

const received_news = [
  {
    _id: 1,
    status: "read",
    image: ProductProposalIMG,
    news_date: "19/05/2020",
    news_title: "bolsa",
    news_text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec maximus consequat ligula eget varius. Donec gravida dictum erat, quis placerat mauris luctus vitae.",
  },
  {
    _id: 2,
    status: "read",
    image: ProductProposalIMG,
    news_date: "18/05/2020",
    news_title: "calça",
    news_text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec maximus consequat ligula eget varius. Donec gravida dictum erat, quis placerat mauris luctus vitae.",
  },
  {
    _id: 3,
    status: "read",
    image: ProductProposalIMG,
    news_date: "17/05/2020",
    news_title: "sapato",
    news_text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec maximus consequat ligula eget varius. Donec gravida dictum erat, quis placerat mauris luctus vitae.",
  },
];

class Messenger extends Component {
  constructor(props) {
    super(props);
    this.chatMessengerChild = React.createRef();
    this.state = {
      chatMessenger: true,
      toggleClassName: false,
    };
  }

  showMessenger = () => {
	  
    const currentClass = this.state.true;
    this.chatMessengerChild.current.showMessenger();

    this.setState({
      toggleClassName: !currentClass
    });
  };

  closeMessenger = () => {
	this.chatMessengerChild.current.closeMessenger();
  }

  render() {
    return (
      <section id="view_messenger">
        <section className="_wrap-messages">
          <Tabs defaultActiveKey="mensagens" onClick={this.closeMessenger}>
            <Tab eventKey="mensagens" title="Mensagens" id="count_messages">
              <div className="_action-buttons">
                <button type="button" className="btn">
                  Marcar como lido
                  <FontAwesomeIcon icon={faEnvelopeOpen}></FontAwesomeIcon>
                </button>
                <button type="button" className="btn">
                  Excluir
                  <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                </button>
              </div>

              {received_messages.map(
                (
                  message,
                  {
                    _id,
                    status,
                    image,
                    message_date,
                    username,
                    action,
                    product_title,
                  }
                ) => (
                  <Message
                    onClick={this.showMessenger}
                    key={message._id}
                    status={message.status}
                    image={message.image}
                    message_date={message.message_date}
                    username={message.username}
                    action={message.action}
                    product_title={message.product_title}
                  ></Message>
                )
              )}
            </Tab>

            <Tab eventKey="noticias" title="Noticias" id="count_news">
              {received_news.map(
                (
                  news,
                  { _id, status, image, news_date, news_title, news_text }
                ) => (
                  <News
                    key={news._id}
                    status={news.status}
                    image={news.image}
                    news_date={news.news_date}
                    news_title={news.news_title}
                    news_text={news.news_text}
                  ></News>
                )
              )}
            </Tab>
          </Tabs>
        </section>

        {this.state.chatMessenger ? (
          <Chat
            ref={this.chatMessengerChild}
            className={
              this.state.toggleClassName ? "_wrap-messenger _max-width" : null
            }
          ></Chat>
        ) : null}
      </section>
    );
  }
}

export default Messenger;
