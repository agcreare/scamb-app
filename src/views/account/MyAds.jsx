import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Tabs, Tab } from 'react-bootstrap';
import Layout from '../../components/account/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faEdit } from '@fortawesome/pro-light-svg-icons';


//Images
import ProductIMG from '../../assets/images/product-favorite.png';
import AvatarIMG from '../../assets/images/avatar.png';

class MyAds extends Component {
	
	render() {
		
		return (
			
			<Layout>
				<div id="my-ads" className="_content">

					<Tabs defaultActiveKey="publicados">
					  <Tab eventKey="publicados" title="Publicados">

						  <div className="_wrap-my-shopping-item">

							  <section className="_my-shopping-item">

								  <div className="_col _col-1">
									<div className="product">
										<div className="product-image">
											<img src={ProductIMG} alt="" />
										</div>
										<div className="product-info">

											<div className="product-info-title _14px _fw700 _color _black">
												Calça
											</div>

											<div className="_product-price _1x">

												<div className="ui-item__price">
													<span class="price-tag-fraction">2099</span>
													<span class="price-tag-symbol">pts</span>
												</div>

											</div>

											<div className="published-in">
												Publicado em: <span className="the-date">19/12/2019</span>
											</div>

										</div>
									</div>
								  </div>

								  <div className="_col _col-2">
									  <h1 className="proposals-received-title _14px _fw700 _color _black">
										  Propostas recebidas
										  <span>( 4 )</span>
									  </h1>
								  </div>

								  <div className="_col _col-3">
									  <div className="_action-buttons">
										<button type="button" className="btn">
											Editar
											<FontAwesomeIcon icon={faEdit}></FontAwesomeIcon>
										</button>
									</div>
								  </div>

							  </section>

							  <sections className="_recent_messages">

								  <Link to="/" className="_wrap-message">
									<div className="contact-infos">
										<span className="user-image _rounded-image__with-notification-icon _medium">
											<img src={AvatarIMG} alt="Avatar" />
										</span>

										<div className="contact-informations">
											<div className="contact-name">
												Fernanda Souza
											</div>
											<div className="contact-message-preview">
												Quanto tempo de uso ela tem?
											</div>
										</div>
									</div>

									 <div className="message-date">
										17/02/2020 
									 </div>
								  </Link>
								  <Link to="/" className="_wrap-message">
									<div className="contact-infos">
										<span className="user-image _rounded-image__with-notification-icon _medium">
											<img src={AvatarIMG} alt="Avatar" />
										</span>

										<div className="contact-informations">
											<div className="contact-name">
												Fernanda Souza
											</div>
											<div className="contact-message-preview">
												Quanto tempo de uso ela tem?
											</div>
										</div>
									</div>

									 <div className="message-date">
										17/02/2020 
									 </div>
								  </Link>

							  </sections>

							  <section className="_load-more-messages">
								  <Link to="/" className="_load-more">
									Ver mais (7)
								  </Link>
							  </section>

						  </div>

					  </Tab>
					  <Tab eventKey="inativos" title="Inativos">

						  <div className="_wrap-my-shopping-item">

							  <section className="_my-shopping-item _inactive">

								  <div className="_col _col-1">
									<div className="product">
										<div className="product-image">
											<img src={ProductIMG} alt="" />
										</div>
										<div className="product-info">

											<div className="product-info-title _14px _fw700 _color _black">
												Calça
											</div>

											<div className="_product-price _1x">

												<div className="ui-item__price">
													<span class="price-tag-fraction">2099</span>
													<span class="price-tag-symbol">pts</span>
												</div>

											</div>

											<div className="published-in">
												Publicado em: <span className="the-date">19/12/2019</span>
											</div>

										</div>
									</div>
								  </div>

								  <div className="_col _col-2">
									  <h1 className="proposals-received-title _14px _fw700 _color _black">
										  Propostas recebidas
										  <span>( 4 )</span>
									  </h1>
								  </div>

								  <div className="_action-inactive">
									  <div className="_action-buttons">
										<button type="button" className="btn">
											Editar
											<FontAwesomeIcon icon={faEdit}></FontAwesomeIcon>
										</button>
									</div>
								  </div>

							  </section>

						  </div>

					  </Tab>
					</Tabs>

				</div>
			</Layout>
				
		);
		
	};
	
};

export default MyAds;