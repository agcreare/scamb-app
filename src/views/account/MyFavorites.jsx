import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Layout from '../../components/account/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faTimes, faCheck } from '@fortawesome/pro-light-svg-icons';

//Images
import ProductIMG from '../../assets/images/product-favorite.png';

class MyFavorites extends Component {
	
	render() {
		
		return (
			
			<div id="view_my-favorites">
				
				<Layout>
					<div id="favorites" className="_content">
						<h1 className="_title-box">Meus Favoritos</h1>
						
						<div className="_wrap-favorites">
							<div className="_action-buttons">
								<button type="button" className="btn">
									Excluir
									<FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
								</button>
							</div>
							
							<div className="_favorites-wrap-list">
								<nav className="_favorites-list">
									<ul>
										<li>
											<div className="action">
												<div className="_checkbox form-check">
													<label className="form-check-label">

														<input type="checkbox" className="form-check-input" />
														<span className="check">
														<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
														</span>

													</label>
												</div>
											</div>
											
											<Link to="/produto" className="product">
												<div className="product-image">
													<img src={ProductIMG} alt=""></img>
												</div>
												<div className="product-info">
													<div className="product-info-title">
														Conjunto Blusa e Shorts
													</div>
													<div className="_product-price _1x">
														<div className="ui-item__discount-price">
															<span class="price-tag-fraction">2099</span>
															<span class="price-tag-symbol">pts</span>
														</div>
														<div className="ui-item__price">
															<span class="price-tag-fraction">2099</span>
															<span class="price-tag-symbol">pts</span>
														</div>

													</div>
												</div>
											</Link>
											
										</li>
										<li>
											<div className="action">
												<div className="_checkbox form-check">
													<label className="form-check-label">

														<input type="checkbox" className="form-check-input" />
														<span className="check">
														<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
														</span>

													</label>
												</div>
											</div>
											
											<Link to="/produto" className="product">
												<div className="product-image">
													<img src={ProductIMG} alt=""></img>
												</div>
												<div className="product-info">
													<div className="product-info-title">
														Conjunto Blusa e Shorts
													</div>
													<div className="_product-price _1x">
														<div className="ui-item__discount-price">
															<span class="price-tag-fraction">2099</span>
															<span class="price-tag-symbol">pts</span>
														</div>
														<div className="ui-item__price">
															<span class="price-tag-fraction">2099</span>
															<span class="price-tag-symbol">pts</span>
														</div>

													</div>
												</div>
											</Link>
											
										</li>
										<li>
											<div className="action">
												<div className="_checkbox form-check">
													<label className="form-check-label">

														<input type="checkbox" className="form-check-input" />
														<span className="check">
														<FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
														</span>

													</label>
												</div>
											</div>
											
											<Link to="/produto" className="product">
												<div className="product-image">
													<img src={ProductIMG} alt=""></img>
												</div>
												<div className="product-info">
													<div className="product-info-title">
														Conjunto Blusa e Shorts
													</div>
													<div className="_product-price _1x">
														<div className="ui-item__discount-price">
															<span class="price-tag-fraction">2099</span>
															<span class="price-tag-symbol">pts</span>
														</div>
														<div className="ui-item__price">
															<span class="price-tag-fraction">2099</span>
															<span class="price-tag-symbol">pts</span>
														</div>

													</div>
												</div>
											</Link>
											
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</Layout>
				
			</div>
			
		);
		
	};
	
};

export default MyFavorites;