import React, { Component } from 'react';
import Layout from '../../components/account/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import { faEdit } from '@fortawesome/pro-light-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/pro-solid-svg-icons';

class MyProfile extends Component {

	componentDidMount() {

		let enableFieldsAddress = $('._form._my-address').find('.form-group > .form-control');
		let enableFieldsPersonal = $('._form._change-personal-data').find('.form-group > .form-control');

		$('#change-address').on('click', function () {
			enableFieldsAddress.prop('disabled', false);
		});

		$(enableFieldsAddress).keypress(function () {
			$('._form._my-address').find('.form-group-button').removeClass('_is-hidden');
		});

		$('#change-personal-data').on('click', function () {
			enableFieldsPersonal.prop('disabled', false);
		});

		$(enableFieldsPersonal).keypress(function () {
			$('._form._change-personal-data').find('.form-group-button').removeClass('_is-hidden');
		});
	}
	
	render() {
		
		return (
			<Layout className="_profile-width">
		
				<div id="my-profile" className="_content">

					<div className="_wrap-form-profile">
						
						<div className="_wrap-form-header">
							<h1 className="_title-box">Meu Perfil</h1>
						</div>
							
						<form className="_form">

							<div className="form-group">
								<label className="_label">Titulo</label>
								<input type="text" className="form-control" placeholder="Digite um titulo para sua loja" defaultValue="Minha Loja" />
							</div>
							
							<div className="form-group">
								<label className="_label">Bio</label>	
								<span className="_character-counter">5 de 350</span> 
								<textarea className="form-control" placeholder="Descrição" rows="3">
									
								</textarea>
							</div>
							
							<div className="form-group">
								<label className="_label">URL Personalizada</label>
								
								<div className="_wrap-custom-url">
									<div className="custom-url">
										scamb.com.br/
									</div>
									<input type="text" className="form-control" placeholder="minhalojinha" defaultValue="jacksoncorrea" />
								</div>
								<small className="form-text text-muted">São permitidos apenas caracteres, numeros e<br/> sinais de - (hífen)  e _ (underline) .</small>
							</div>
							
							<div className="form-group-button">
								<button type="submit" className="btn btn-red">Confirmar alterações</button>
							</div>	
							
						</form>
						
					  </div>

					<div className="_wrap-form-profile">
						
						<div className="_wrap-form-header">
							<h1 className="_title-box">Dados Pessoais</h1>
							
							<button type="button" className="btn btn-transparent _edit-profile" id="change-personal-data">
								<FontAwesomeIcon icon={faEdit}></FontAwesomeIcon>
							</button>
							
						</div>
							
						<form className="_form _change-personal-data">

							<div className="form-group">
								<label className="_label">Nome</label>
								<input type="text" className="form-control" placeholder="Seu nome completo" defaultValue="Emilia Clarke" disabled="disabled"/>
							</div>

							<div className="form-group">
								<label className="_label">Email</label>
								<input type="email" className="form-control" placeholder="Seu melhor e-mail" defaultValue="emiliaclarke@gmail.com" disabled="disabled"/>
							</div>

							<div className="form-group">
								<label className="_label">Senha</label>
								<input type="password" className="form-control" placeholder="Sua senha" defaultValue="123456789" disabled="disabled"/>
							</div>

							<div className="form-group">
								<label className="_label">Cpf</label>
								<input type="text" className="form-control" placeholder="Seu CPF" defaultValue="000-000-000-00" disabled="disabled"/>
							</div>

							<div className="form-group">
								<label className="_label">Telefone</label>
								<input type="text" className="form-control" placeholder="Seu Telefone Fixo ou Celular" defaultValue="(00) 000 - 000" disabled="disabled"/>
							</div>
							
							<div className="form-group-button _is-hidden">
								<button type="submit" className="btn btn-red">Confirmar alterações</button>
							</div>	
							
						</form>
						
					  </div>

					<div className="_wrap-form-profile">
						
						<div className="_wrap-form-header">
							<h1 className="_title-box">Endereço</h1>
							
							<button type="button" className="btn btn-transparent _edit-profile" id="change-address">
								<FontAwesomeIcon icon={faEdit}></FontAwesomeIcon>
							</button>
							
						</div>
						
						<div className="_set-user-location">

                            <div className="_wrap-location">

                                <div className="_icon">
                                    <FontAwesomeIcon icon={faMapMarkerAlt}></FontAwesomeIcon>
                                </div>

                                <div className="_wrap-current-location">
                                    <div className="_location-title">
                                        CEP: 85050-030
                                    </div>
                                    <div className="_location">
                                        R Bartolomeu bueno da silva,
                                        n359 - Bairro Água Verde,
                                        Curitiba - Paraná
                                    </div>
                                </div>
                            </div>
                        </div>
							
						<form className="_form _my-address">

							<div className="form-group">
								<label className="_label">Cep</label>
								<input type="text" className="form-control" placeholder="Informe seu cep" disabled="disabled" />
								<small className="form-text text-muted">Não sabe seu cep? <a rel="noopener noreferrer" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">clique aqui</a></small>
							</div>

							<div className="form-group">
								<label className="_label">Estado</label>
								<input type="email" className="form-control" placeholder="Qual seu estado?" disabled="disabled" />
							</div>

							<div className="form-group">
								<label className="_label">Cidade</label>
								<input type="text" className="form-control" placeholder="Qual sua cidade?" disabled="disabled" />
							</div>

							<div className="form-group">
								<label className="_label">Endereço</label>
								<input type="text" className="form-control" placeholder="Seu CPF" disabled="disabled" />
							</div>

							<div className="form-group">
								<label className="_label">Número</label>
								<input type="text" className="form-control" placeholder="Número" disabled="disabled" />
							</div>

							<div className="form-group">
								<label className="_label">Complemento</label>
								<input type="text" className="form-control" placeholder="..." disabled="disabled" />
							</div>

							<div className="form-group">
								<label className="_label">Bairro</label>
								<input type="text" className="form-control" placeholder="Qual o bairro?" disabled="disabled" />
							</div>
							
							<div className="form-group-button _is-hidden">
								<button type="submit" className="btn btn-red">Confirmar alterações</button>
							</div>	
							
						</form>
						
					  </div>

				</div>

			</Layout>
		);
		
	};
	
};

export default MyProfile;