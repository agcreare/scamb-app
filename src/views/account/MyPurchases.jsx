import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Layout from '../../components/account/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import $ from 'jquery';

// Icons
import { faEllipsisV } from '@fortawesome/pro-light-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/pro-solid-svg-icons';


//Images
import ProductIMG from '../../assets/images/product-favorite.png';
import AvatarIMG from '../../assets/images/avatar.png';


class MyShopping extends Component {
	
	componentDidMount() {
		var options = $('._options');

		$(options).on('click', function () {
			$('#dd-purchase').toggleClass('_is-hidden');
		});
	}	

	render() {
		
		return (
			
			<Layout>
				<div id="my-purchases" className="_content">
					
					<h1 className="_title-box">Minhas compras</h1>
					
					<div className="_all-purchases">
						<div className="_purchase-item">

							<div className="_product">
								<div className="_product-image">
									<img src={ProductIMG} alt="" />
								</div>

								<div className="_wrap-product-info">

									<div className="product-info-title _14px _fw700 _color _black">
										Calça
									</div>

									<div className="_product-price _1x">

										<div className="ui-item__price">
											<span class="price-tag-fraction">2099</span>
											<span class="price-tag-symbol">pts</span>
										</div>

									</div>

									<div className="tag-date">
										Comprado em: <span className="the-date">19/12/2019</span>
									</div>

									<div className="tag-date">
										Entregue em: <span className="the-date">19/12/2019</span>
									</div>

									<div className="_delivery-address">
										<h1 className="_delivery-address-title _12px _color _black _fw700">
											Endereço de entrega
										</h1>

										<div className="localtion-address">
											<div className="icon">
												<FontAwesomeIcon icon={faMapMarkerAlt}></FontAwesomeIcon>
											</div>
											<div className="address">
												R Bartolomeu bueno da silva, n359 - Bairro primavera, Guarapuava - Parana
											</div>
										</div>
									</div>

								<div className="_seller">
									<div className="_seller-image">
										<span className="user-image _rounded-image__with-notification-icon _medium">
											<img src={AvatarIMG} alt="Avatar" />
										</span>
									</div>
									<div className="_seller-info">
										<div className="_seller-info-title _12px _color _black">
											Vendedor
										</div>
										<div className="_seller-info-name _13px _color _gray _fw700">
											Jackson Correa
										</div>
										<Link to="/" className="open-messages">
											Ver Mensagens
										</Link>
									</div>
								</div>
								</div>
							</div>

							<div className="_actions">
								<button type="button" className="btn btn-transparent _options">
									<FontAwesomeIcon icon={faEllipsisV}></FontAwesomeIcon>
								</button>
								
								<div className="_dropdown-menu _is-hidden" id="dd-purchase">
                                    <nav className="_list-nav">
                                        <ul>
                                            <li>
                                                <Link to="/">
                                                    Tenho um problema
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    Enviar mensagem ao vendedor
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    Cancelar compra
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    Ver status do pedido
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
							</div>
						</div>
					</div>
					
				</div>
			</Layout>
			
		);
		
	};
	
};

export default MyShopping;