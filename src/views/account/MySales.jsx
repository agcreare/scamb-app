import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Layout from '../../components/account/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {Accordion, Card, Button} from 'react-bootstrap';

// Icons
import { faChevronDown, faInbox } from '@fortawesome/pro-light-svg-icons';
import { faCircle } from '@fortawesome/pro-solid-svg-icons';


//Images
import ProductIMG from '../../assets/images/product-favorite.png';
import AvatarIMG from '../../assets/images/avatar.png';
import Tenis from '../../assets/images/tenis-lacoste.png';

class MySales extends Component {
	
	render() {
		
		return (
			
			<Layout>
				<div id="my-sales" className="_content">
					
					<h1 className="_title-box">Minhas Vendas</h1>
					
					<div className="_all-sales">
                        
                        <Accordion defaultActiveKey="0" className="_accordion">
                          <Card>
                            <Card.Header>
                              <Accordion.Toggle as={Button} variant="link" eventKey="0">

                                <div className="_product">
                                    <div className="_product-image">
                                        <img src={ProductIMG} alt="" />
                                    </div>

                                    <div className="_wrap-product-info">

                                        <div className="product-info-title _14px _fw700 _color _black">
                                            Calça
                                        </div>

                                        <div className="_product-price _1x">

                                            <div className="ui-item__price">
                                                <span class="price-tag-fraction">98</span>
                                                <span class="price-tag-symbol">pts</span>
                                            </div>

                                        </div>

                                        <div className="tag-date">
                                            Vendido em: <span className="the-date">19/12/2019</span>
                                        </div>

                                    </div>
                                </div>
                                  
                                <button type="button" className="btn btn-transparent toggle-arrow">
                                    <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>  
                                </button>  
                                  
                                {/*
                                    status: delivered, canceled, waiting
                                */}
                                <div className="payment-status delivered">
                                    Entregue
                                    <FontAwesomeIcon icon={faCircle}></FontAwesomeIcon>  
                                </div>  
                              </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    
                                    <section className="_sales-info">
                                        
                                        <div className="_wrap-info">
                                            <div className="_info">

                                                <div className="_info-item">
                                                    <div className="_info-item-title">Número do pedido</div>
                                                    6EA69689WW944200R
                                                </div>

                                                <div className="_info-item">
                                                    <div className="_info-item-title">Código de rastreamento</div>
                                                    <a href="/">AA123456789BR</a>
                                                </div>

                                                <div className="_info-item">
                                                    <div className="_info-item-title">Valor da venda</div>
                                                    <span className="_points">98</span> pontos
                                                </div>

                                            </div>
                                            
                                            <div className="_total">
                                                <div className="_total-text">
                                                    Total recebido
                                                </div>
                                                <div className="_total-points">
                                                    <span className="_points">98</span> pontos
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div className="_wrap-seller">
                                            <div className="_seller">
                                                <div className="_seller-image">
                                                    <span className="user-image _rounded-image__with-notification-icon _2x">
                                                        <img src={AvatarIMG} alt="Avatar" />
                                                    </span>
                                                </div>
                                                <div className="_seller-info">
                                                    <div className="_seller-info-title">
                                                        comprador
                                                    </div>
                                                    <div className="_seller-info-name">
                                                        Emilia Clarke
                                                    </div>
                                                    <Link to="/mensagens" className="open-messages">
                                                        <div className="_action-buttons">
                                                            <button type="button" className="btn">
                                                                ver conversas
                                                                <FontAwesomeIcon icon={faInbox}></FontAwesomeIcon>
                                                            </button>
                                                        </div>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    
                                </Card.Body>
                            </Accordion.Collapse>
                          </Card>
                        </Accordion>
                        
					</div>
					
				</div>
			</Layout>
			
		);
		
	};
	
};

export default MySales;