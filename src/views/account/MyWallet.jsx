import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Layout from '../../components/account/Layout';
import CreditCard from '../../components/account/wallet/CreditCard';
import ActivityHistory from '../../components/account/wallet/ActivityHistory';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Icons
import { faEllipsisV, faClone, faLongArrowRight, faCalendarAlt, faSearch } from '@fortawesome/pro-light-svg-icons';
import Visa from '../../assets/images/credit-card/visa.png';
import Mastercard from '../../assets/images/credit-card/mastercard.png';
import AmericanExpress from '../../assets/images/credit-card/american-express.png';
import DinersClub from '../../assets/images/credit-card/diners-club.png';


class MyWallet extends Component {
    
    render() {
        
        return (
        
            <div id="view_my-wallet">
                
                <Layout>
                    
                    <div id="_my-wallet">
                        
                        <h1 className="_title-box">Minha Carteira</h1>
                        
                        <div className="_wrap-cards _flex">
                            
                            <div className="ui-card _medium">
                                <div className="ui-card-head _with-icon">
                                    
                                    <h1 className="ui-card-title">
                                        Meus Pontos
                                    </h1>
                                    
                                    <button type="button" className="btn btn-transparent">
                                        <FontAwesomeIcon icon={faEllipsisV}></FontAwesomeIcon>
                                    </button>
                                    
                                </div>
                                
                                <div className="ui-card-content">
                                    
                                    <h1 className="my-points">
                                        <div className="_product-price _2x">
                                            <div className="ui-item__price">
                                                <span class="price-tag-fraction">600</span>
                                                <span class="price-tag-symbol">pts</span>
                                            </div>
                                        </div>
                                    </h1>
                                    
                                    <div className="add-points">
                                        
                                        <h1 className="add-points-title">
                                            adicione pontos a sua carteira
                                        </h1>

                                        <div className="add-points-content">
                                            
                                            <ul className="_payment-methods-list">
                                                <li>
                                                    <div className="creadit-card--flag">
                                                        <img src={Visa}  alt="" />
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="creadit-card--flag">
                                                        <img src={Mastercard}  alt="" />
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="creadit-card--flag">
                                                        <img src={AmericanExpress}  alt="" />
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="creadit-card--flag">
                                                        <img src={DinersClub}  alt="" />
                                                    </div>
                                                </li>
                                            </ul>
                                            
                                            <Link className="link-button" to="/">Comprar mais pontos</Link>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                            <div className="ui-card _medium _invite">
                                <div className="ui-card-head _with-icon">
                                    
                                    <h1 className="ui-card-title">
                                        Convide amigos e ganhe pontos
                                    </h1>
                                    
                                </div>
                                
                                <div className="ui-card-content">
                                    <div className="_invite-label">
                                        <form className="_form">
                                            <div className="form-group">
                                                <input type="text" className="form-control" placeholder="https://scamb.com.vc/x45dcz" value="https://scamb.com.vc/x45dcz" />
                                                <button type="button" className="btn">
                                                    <FontAwesomeIcon icon={faClone}></FontAwesomeIcon>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                    <ul className="_guests-list">
                                        <li>
                                            <div className="guests-info">
                                                Usários cadastrados com seu link:
                                                <span className="guests-count">13</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="guests-info">
                                                Valor recebido:
                                                <span className="guests-count">280 Pontos</span>
                                            </div>
                                        </li>
                                    </ul>
                                    
                                    <Link className="link-button" to="/">Saber mais</Link>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                        
                        <div className="_wrap-cards">
                            
                            <div className="ui-card _fluid _cc">
                                <div className="ui-card-head _with-icon">
                                    
                                    <h1 className="ui-card-title">
                                        Meus cartões de crédito
                                    </h1>
                                    
                                </div>
                                
                                <div className="ui-card-content">
                                    
                                    <CreditCard></CreditCard>
                                    
                                </div>
                            </div>
                        </div>
                        
                        <div className="_wrap-cards">
                            
                            <div className="ui-card _fluid _with-icon activity-history">
                                <div className="ui-card-head _with-icon">
                                    
                                    <h1 className="ui-card-title">
                                        Histórico de atividades
                                    </h1>
                                    
                                    <Link to="/historico-de-atividades" className="btn btn-transparent">
                                        <FontAwesomeIcon icon={faLongArrowRight}></FontAwesomeIcon>
                                    </Link>
                                    
                                </div>
                                
                                <div className="ui-card-content">
                                    
                                    <ActivityHistory></ActivityHistory>
                                    <section className="_load-more-activities">
                                        <Link to="/historico-de-atividades" className="link-button _load-more">Ver histórico completo</Link>
                                    </section>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </Layout>
                
            </div>
            
        );
        
    };
    
};

export default MyWallet;