import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


// Icons
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faCheck } from '@fortawesome/pro-light-svg-icons';


class CreateAccount extends Component {
	
	render() {
		
		return (
			
			<div id="view_create-account">
				
				<section className="_wrap-auth-screen">
					
					<div className="auth-screen">
						
						<h1 className="_auth-screen-title headline _color _black _fw700 text-center">
							Acesse sua conta Scamb
						</h1>
						
						<div className="_wrap-auth-facebook">
							<Link to="/" className="_auth-facebook-btn">
								<span className="btn-icon">
									<FontAwesomeIcon icon={faFacebookF}></FontAwesomeIcon>
								</span>
								Entrar com o facebook
							</Link>
						</div>
						
						<div className="_or">
							<div className="divider _color _gray _13px">
								<span className="text">ou</span>
							</div>
						</div>
						
						
						<div className="_wrap-form">
							<form className="_form">
							  <div className="form-group">
								<input type="text" className="form-control" placeholder="Nome completo" />
							  </div>
							  <div className="form-group">
								<input type="email" className="form-control" placeholder="E-Mail" />
							  </div>
							  <div className="form-group">
								<input type="password" className="form-control" placeholder="Senha" />
							  </div>
							  <div className="form-group">
								<input type="password" className="form-control" aria-describedby="passwordHelp" placeholder="Repita sua senha" />
							    <small id="passwordHelp" className="form-text text-muted">Máximo de 6 caracteres</small>
							  </div>
							  <div className="_checkbox form-check">
								  <label className="form-check-label">
									  
									<input type="checkbox" className="form-check-input" />
									  <span className="check">
									   <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
									</span>

									<div className="_alt-text">
										Ao clicar em criar conta, voce está de acordo com os nossos <Link to="/">termos de uso</Link>  	
									</div>
								  
								  </label>
								
							  </div>
								
								<div className="form-group-button">
									<button type="submit" className="btn btn-primary">Fazer login</button>
								</div>	
							</form>
						</div>
						
						<div className="_have-account _color _gray _13px _fw700 text-center">
							Já possui uma conta? <Link to="/login">Faça o login</Link>
						</div>
						
					</div>
					
				</section>
				
			</div>
			
		);
		
	};
	
};

export default CreateAccount;