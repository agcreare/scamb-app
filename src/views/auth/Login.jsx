import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


// Icons
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faCheck } from '@fortawesome/pro-light-svg-icons';


class Login extends Component {
	
	render() {
		
		return (
			
			<div id="view_login">
				
				<section className="_wrap-auth-screen">
					
					<div className="auth-screen">
						
						<h1 className="_auth-screen-title headline _color _black _fw700 text-center">
							Acesse sua conta Scamb
						</h1>
						
						<div className="_wrap-auth-facebook">
							<Link to="/" className="_auth-facebook-btn">
								<span className="btn-icon">
									<FontAwesomeIcon icon={faFacebookF}></FontAwesomeIcon>
								</span>
								Entrar com o facebook
							</Link>
						</div>
						
						<div className="_or">
							<div className="divider _color _gray _13px">
								<span className="text">ou</span>
							</div>
						</div>
						
						
						<div className="_wrap-form">
							<form className="_form">
								
							  <div className="form-group">
								<input type="email" className="form-control" placeholder="E-Mail" />
							  </div>
								
							  <div className="form-group">
								<input type="password" className="form-control" placeholder="Senha" />
							  </div>
								
							  <div className="_checkbox form-check">
								  <label className="form-check-label">
									  
									<input type="checkbox" className="form-check-input" />
									  <span className="check">
									   <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
									</span>

									<div className="_alt-text">
										Me mantenha logado	
									</div>
								  
								  </label>
								
							  </div>
								
								<div className="form-group-button">
									<button type="submit" className="btn btn-primary">Fazer login</button>
								</div>	
							</form>
						</div>
						
						<div className="_have-account _color _gray _13px _fw700 text-center">
							Ainda não possui uma conta? <Link to="/criar-conta">Criar Conta</Link>
						</div>
						
					</div>
					
				</section>
				
			</div>
			
		);
		
	};
	
};

export default Login;