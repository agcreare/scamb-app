import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Welcome extends Component {
	
	render() {
		
		return (
			
			<div id="view_access">
				
				<section className="_wrap-access-screen">
					
					<div className="access-screen">
						
						<h1 className="_access-screen-title">
							Bem vindo
							<span className="heading">
								Antes de continuarmos voce precisa criar uma conta. fica tranquilx é super rápido!
							</span>
						</h1>
						
						<Link to="/login" className="btn btn-primary">Crie sua conta</Link>
						
						<div className="_have-account _color _black _fw700">
							Já possui uma conta? <Link to="/login">Faça o login</Link>
						</div>
						
					</div>
					
				</section>
				
			</div>
			
		);
		
	};
	
};

export default Welcome;