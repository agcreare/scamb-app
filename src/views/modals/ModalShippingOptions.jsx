import React, {Component, useState} from 'react';
import {Accordion, Card, Button} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//Icons
import { faChevronDown } from '@fortawesome/pro-light-svg-icons';

class ModalTaxs extends Component {

    render () {
    
      return (
        <>
            <Accordion className="_accordion small">
              <Card>
                <Card.Header>
                  <Accordion.Toggle as={Button} variant="link" eventKey="0">

                    <div className="_accordion-title">
                        Operodor logístico
                    </div>
                    
                    <button type="button" className="btn btn-transparent toggle-arrow">
                        <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>  
                    </button>  
                      
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="0">
                    <Card.Body>
                        
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae lorem vitae mi aliquet fringilla a id est. Donec quis leo eu massa vestibulum finibus.
                        
                    </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Card.Header>
                  <Accordion.Toggle as={Button} variant="link" eventKey="1">

                    <div className="_accordion-title">
                    Aplicativo de entrega
                    </div>
                    
                    <button type="button" className="btn btn-transparent toggle-arrow">
                        <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>  
                    </button>  
                      
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="1">
                    <Card.Body>
                        
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae lorem vitae mi aliquet fringilla a id est. Donec quis leo eu massa vestibulum finibus.
                        
                    </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Card.Header>
                  <Accordion.Toggle as={Button} variant="link" eventKey="2">

                    <div className="_accordion-title">
                    Entrega em mãos
                    </div>
                    
                    <button type="button" className="btn btn-transparent toggle-arrow">
                        <FontAwesomeIcon icon={faChevronDown}></FontAwesomeIcon>  
                    </button>  
                      
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="2">
                    <Card.Body>
                        
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae lorem vitae mi aliquet fringilla a id est. Donec quis leo eu massa vestibulum finibus.
                        
                    </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
        </>
      );
    
}
}

export default ModalTaxs;