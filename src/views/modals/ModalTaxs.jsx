import React, {Component, useState} from 'react';

class ModalTaxs extends Component {

    render () {
    
      return (
        <>
            <ul className="_taxs-list">
                <li>
                    <div className="_taxs-list-title">Valor da venda</div>
                    <div className="_taxs-list-value">
                        <div className="_value">Taxa</div>
                    </div>
                </li>
                <li>
                    <div className="_taxs-list-title">1 - 49 Pontos</div>
                    <div className="_taxs-list-value">
                        <div className="_value">R$2,90</div>
                    </div>
                </li>
                <li>
                    <div className="_taxs-list-title">50 - 99 Pontos</div>
                    <div className="_taxs-list-value">
                        <div className="_value">R$4,90</div>
                    </div>
                </li>
                <li>
                    <div className="_taxs-list-title">100 - 249 Pontos</div>
                    <div className="_taxs-list-value">
                        <div className="_value">R$ 6,90</div>
                    </div>
                </li>
                <li>
                    <div className="_taxs-list-title">200 - 299 Pontos</div>
                    <div className="_taxs-list-value">
                        <div className="_value">R$ 8,90</div>
                    </div>
                </li>
                <li>
                    <div className="_taxs-list-title">Acima de 500 Pontos</div>
                    <div className="_taxs-list-value">
                        <div className="_value">R$ 9,90</div>
                    </div>
                </li>
            </ul>
        </>
      );
    
}
}

export default ModalTaxs;